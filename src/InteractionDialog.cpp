/*
 * InteractionDialog.cpp
 *
 * Copyright (C) 2003 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */
 
#include "InteractionDialog.h"
#include "resource.h"
#include "FieldValue.h"
#include "SFMFTypes.h"
#include "Field.h"
#include "EventIn.h"
#include "EventOut.h"
#include "ExposedField.h"
#include "Element.h"
#include "DuneApp.h"
#include "NodeTransform.h"

InteractionDialog::InteractionDialog(SWND parent, Node* oldNode)
  : Dialog(parent, IDD_INTERACTION)
{
    _window.initCheckBoxWindow(parent, _dlg);
    _interactionNode = oldNode;
    _level = 0;
    buildInterfaceData();
    LoadData();
    _window.accountYmax();
    _window.invalidateWindow();
}

void
InteractionDialog::buildInterfaceData(void)
{
    int i;
    Scene *scene = _interactionNode->getScene();
    bool x3d = scene->isX3d();
    Proto* inProto = _interactionNode->getProto();
    if (inProto == NULL)
        return;

    _sensor = NULL;
    int sensor = swComboBoxGetSelection(
                       swGetDialogItem(_dlg, IDC_INTERACTION_SENSORS));

    bool _eventInHasRecommendedFields = false;
    for (i = 0; i < inProto->getNumEventIns(); i++)
        if (inProto->getEventIn(i)->getFlags() &&
            EIF_RECOMMENDED)
            _eventInHasRecommendedFields = true;

    if (sensor > 0) {
        const NodeList *nodes = _interactionNode->getScene()->getNodes();
        for (i = 0; i < nodes->size() && (_sensor == NULL); i++) {
            Node    *node = nodes->get(i);
            if (node->isInScene(_interactionNode->getScene()))
               if (node->hasName())
                  if (strcmp(_sensors[sensor], node->getName()) == 0) {
                      _sensor = node;
                      break;
                  }
        }
    }
    _routeData.resize(0);
    _window.resize0();
    int index = 0;
    for (i = 0; i < inProto->getNumEventIns(); i++) {
        int type = inProto->getEventIn(i)->getType();
        // when _level == 0 filter out not recommended fields
        int inFlags = inProto->getEventIn(i)->getFlags();
        if ((_level == 0) && _eventInHasRecommendedFields &&
            !(inFlags && EIF_RECOMMENDED))
            continue;
        if ((inFlags & FF_X3D_ONLY) && !x3d)
            continue;
        if (typeDefaultValue(type)->supportInteraction()) {
            for (int j = 0; j < scene->getNumInteractiveProtos(type); j++) {
                Proto *outProto = scene->getInteractiveProto(type, j);
                if (_sensor != NULL)
                    if (strcmp(outProto->getName(x3d),
                               _sensor->getProto()->getName(x3d)) != 0)
                        continue;
                for (int k = 0; k < outProto->getNumEventOuts(); k++) {
                    // when _level == 0 filter out not recommended fields
                    int outFlags = outProto->getEventOut(k)->getFlags();
                    if ((_level == 0) && !(outFlags && EOF_RECOMMENDED))
                        continue;
                    if ((outFlags & FF_X3D_ONLY) && !x3d)
                        continue;
                    if (outProto->getEventOut(k)->getType() == type) {
                        _routeData[index].proto = outProto;
                        _routeData[index].eventInField = i;
                        _routeData[index].eventOutField = k;
                        _routeData[index].type = type;
                        MyString string = "";
                        string += inProto->getEventIn(i)->getName(x3d);
                        string += " ";
                        string += outProto->getName(x3d);
                        string += ".";
                        string += outProto->getEventOut(k)->getName(x3d);
                        _window.setString(index, string);
                        index++; 
                    }
                }
            }
        }
    }
    for (i = 0; i < _routeData.size(); i++)
        _window.setInitButtonsPressed(i, false);
}

void
InteractionDialog::OnCommand(int id)
{
    if (id == IDOK) {
        SaveData();
        if (Validate()) {
            swEndDialog(IDOK);
        }
    } else if (id == IDC_INTERACTION_SELECT_LEVEL) {
        _level = swComboBoxGetSelection(
                       swGetDialogItem(_dlg, IDC_INTERACTION_LEVEL));
        buildInterfaceData();
        _window.accountYmax();
        _window.invalidateWindow();
    } else if (id == IDC_INTERACTION_SELECT_SENSOR) {
        buildInterfaceData();
        _window.accountYmax();
        _window.invalidateWindow();
    } else if (id == IDC_INTERACTION_LEVEL) {
        _level = swComboBoxGetSelection(
                       swGetDialogItem(_dlg, IDC_INTERACTION_LEVEL));
        buildInterfaceData();
//        _window.accountYmax();
        _window.invalidateWindow();
    } else if (id == IDC_INTERACTION_SENSORS) {
        buildInterfaceData();
//        _window.accountYmax();
        _window.invalidateWindow();
    } else if (id == IDCANCEL) {
        swEndDialog(IDCANCEL);
    }
}

bool
InteractionDialog::Validate()
{
    for (int i = 0 ; i < numRoutes() ; i++)
        if (_eventInIsInteractive[i] == true)
            return true;
    int commentID = _interactionNode->getInteractionCommentID();
    if (commentID != -1)
        TheApp->MessageBoxId(IDS_MAKE_WHAT_INTERACTIVE);
    return false;
}

void
InteractionDialog::LoadData()
{
    int i;

    SWND comboSensors = swGetDialogItem(_dlg, IDC_INTERACTION_SENSORS);
    swComboBoxDeleteAll(comboSensors);

    char newSensor[256];
    swLoadString(IDS_INTERACTION_NEW_SENSOR, newSensor, 255); 
    _sensors[0] = newSensor;

    // account list of node(name)s with EventIns, which can be connected to
    // interactive sensors
    const NodeList *nodes = _interactionNode->getScene()->getNodes();
    for (i = 0; i < nodes->size(); i++) {
        Node *node = nodes->get(i);
        Scene *scene = _interactionNode->getScene();
        bool x3d = scene->isX3d();
        if (node->isInScene(scene))
            if (node->hasName())                
                for (int j = 0; j < _routeData.size(); j++) {
                    int t = _routeData[j].type;
                    for (int k = 0; k < scene->getNumInteractiveProtos(t); k++)
                        if (strcmp(scene->getInteractiveProto(t, k)->
                                   getName(x3d),
                                   node->getProto()->getName(x3d)) == 0) {
                            bool alreadyInSensors = false;
                            for (int l = 0; l < _sensors.size(); l++)
                                if (strcmp(_sensors[l], node->getName()) == 0) {
                                    alreadyInSensors = true;
                                    break;
                            }
                            if (!alreadyInSensors)   
                                _sensors.append(node->getName()); 
                        }
                }    
    }
    
    for (i = 0;i < _sensors.size(); i++)
        swComboBoxAppendItem(comboSensors, _sensors[i]);

    SWND comboLevels = swGetDialogItem(_dlg, IDC_INTERACTION_LEVEL);
    swComboBoxDeleteAll(comboLevels);


    char recommended[256];
    swLoadString(IDS_INTERACTION_LEVEL_RECOMMENDED, recommended, 255); 
    swComboBoxAppendItem(comboLevels, recommended);

    char all[256];
    swLoadString(IDS_INTERACTION_LEVEL_ALL, all, 255); 
    swComboBoxAppendItem(comboLevels, all);
}

void 
InteractionDialog::SaveData()
{
    for (int i = 0 ; i < numRoutes() ; i++)
        _eventInIsInteractive[i] = _window.getChecked(i);
}


/*
void 
InteractionDialog::drawInterface(SDC dc)
{
    _window.drawInterface(dc);
}
*/
