/*
 * NodeGeoElevationGrid.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _NODE_GEO_ELEVATION_GRID_H
#define _NODE_GEO_ELEVATION_GRID_H

#ifndef _GEO_NODE_H
#include "GeoNode.h"
#endif
#ifndef _PROTO_MACROS_H
#include "ProtoMacros.h"
#endif
#ifndef _PROTO_H
#include "Proto.h"
#endif

#include "SFMFTypes.h"

class ProtoGeoElevationGrid : public GeoProto {
public:
                    ProtoGeoElevationGrid(Scene *scene);
    virtual Node   *create(Scene *scene);

    virtual int     getType() const { return VRML_GEO_ELEVATION_GRID; }

    FieldIndex color;
    FieldIndex normal;
    FieldIndex texCoord;
    FieldIndex yScale;
    FieldIndex yScaleX3D;
    FieldIndex ccw;
    FieldIndex colorPerVertex;
    FieldIndex creaseAngle;
    FieldIndex creaseAngleX3D;
    FieldIndex geoGridOrigin;
    FieldIndex geoGridOriginX3D;
    FieldIndex height;
    FieldIndex heightX3D;
    FieldIndex normalPerVertex;
    FieldIndex solid;
    FieldIndex xDimension;
    FieldIndex xSpacing;
    FieldIndex xSpacingX3D;
    FieldIndex zDimension;
    FieldIndex zSpacing;
    FieldIndex zSpacingX3D;
};

class NodeGeoElevationGrid : public GeoNode {
public:
                    NodeGeoElevationGrid(Scene *scene, Proto *proto);

    virtual int     getProfile(void) const { return PROFILE_INTERCHANGE; }
    virtual Node   *copy() const { return new NodeGeoElevationGrid(*this); }

    virtual bool    hasTwoSides(void) { return true; }
    virtual bool    isDoubleSided(void) { return !solid()->getValue(); }
    virtual void    toggleDoubleSided(void)
                       { solid(new SFBool(!solid()->getValue())); }
    virtual int     getSolidField() { return solid_Field(); }
    virtual void    flipSide(void) { ccw(new SFBool(!ccw()->getValue())); }

    virtual Node   *getColorNode() { return color()->getValue(); }

    void            setField(int index, FieldValue *value);
    Node           *convert2Vrml(void);

    fieldMacros(SFNode,   color,            ProtoGeoElevationGrid)
    fieldMacros(SFNode,   normal,           ProtoGeoElevationGrid)
    fieldMacros(SFNode,   texCoord,         ProtoGeoElevationGrid)
    fieldMacros(SFFloat,  yScale,           ProtoGeoElevationGrid)
    fieldMacros(SFFloat,  yScaleX3D,        ProtoGeoElevationGrid)
    fieldMacros(MFFloat,  height,           ProtoGeoElevationGrid)
    fieldMacros(MFDouble, heightX3D,        ProtoGeoElevationGrid)
    fieldMacros(SFBool,   ccw,              ProtoGeoElevationGrid)
    fieldMacros(SFBool,   colorPerVertex,   ProtoGeoElevationGrid)
    fieldMacros(SFFloat,  creaseAngle,      ProtoGeoElevationGrid)
    fieldMacros(SFDouble, creaseAngleX3D,   ProtoGeoElevationGrid)
    fieldMacros(SFString, geoGridOrigin,    ProtoGeoElevationGrid)
    fieldMacros(SFVec3d,  geoGridOriginX3D, ProtoGeoElevationGrid)
    fieldMacros(SFBool,   normalPerVertex,  ProtoGeoElevationGrid)
    fieldMacros(SFBool,   solid,            ProtoGeoElevationGrid)
    fieldMacros(SFInt32,  xDimension,       ProtoGeoElevationGrid)
    fieldMacros(SFString, xSpacing,         ProtoGeoElevationGrid)
    fieldMacros(SFDouble, xSpacingX3D,      ProtoGeoElevationGrid)
    fieldMacros(SFInt32,  zDimension,       ProtoGeoElevationGrid)
    fieldMacros(SFString, zSpacing,         ProtoGeoElevationGrid)
    fieldMacros(SFDouble, zSpacingX3D,      ProtoGeoElevationGrid)
};

#endif // _NODE_GEO_ELEVATION_GRID_H

