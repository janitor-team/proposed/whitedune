/*
 * Field.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "Field.h"
#include "ExposedField.h"
#include "Scene.h"
#include "FieldValue.h"

Field::Field(int type, const MyString &name, FieldValue *value,
             ExposedField *exposedField, 
             FieldValue *min, FieldValue *max, int nodeType, int flags, 
             const char **strings, const MyString &x3dName)
{
    _name = name;
    _x3dName = x3dName;
    _type = type;
    _value = value;
    _x3dValue = NULL;
    if (_value) _value->ref();
    _exposedField = exposedField;
    _min = min;
    if (_min) _min->ref();
    _max = max;
    if (_max) _max->ref();
    _nodeType = nodeType;
    _flags = flags;
    _strings = strings;
    _eventIn = -1;
}

Field::~Field()
{
    if (_value) _value->unref();
    if (_min) _min->unref();
    if (_max) _max->unref();
}

const char * 
Field::getElementName(bool x3d) const
{
    if (x3d)
        return "initializeOnly";
    else
        return "field";
}

int Field::write(int f, int indent, int flags) const
{
    if (_flags & (FF_HIDDEN | FF_STATIC)) return 0;

    if (isX3dXml(flags))
        RET_ONERROR( indentf(f, indent + TheApp->GetIndent()) )

    RET_ONERROR( writeElementPart(f, indent, flags) )
    if (isX3dXml(flags)) {
        bool isNull = (flags & NULL_VALUE);
        if (isNull)
            RET_ONERROR( mywritestr(f, " value='NULL'") )
        bool nodeField = ((_type == SFNODE) || (_type == MFNODE));
        if (flags & WITHOUT_VALUE) {
            RET_ONERROR( mywritestr(f, " />\n") )
            TheApp->incSelectionLinenumber();
        } else if (nodeField) {
            RET_ONERROR( mywritestr(f, ">\n") )
            TheApp->incSelectionLinenumber();
            if (_x3dValue) 
                RET_ONERROR( _x3dValue->writeXml(f, indent) )
            else if (_value) 
                RET_ONERROR( _value->writeXml(f, indent) )
            RET_ONERROR( indentf(f, indent) )
            RET_ONERROR( mywritestr(f, "</field>\n") )
            TheApp->incSelectionLinenumber();
        } else {
            if (_x3dValue) {
                RET_ONERROR( mywritestr(f, " value=") )
                RET_ONERROR( _x3dValue->writeXml(f, 0) )
            } else if (_value) {
                RET_ONERROR( mywritestr(f, " value=") )
                RET_ONERROR( _value->writeXml(f, 0) )
            }
            RET_ONERROR( mywritestr(f, " />\n") )
            TheApp->incSelectionLinenumber();
        }
    } else {
        if (!(flags & WITHOUT_VALUE)) {
            if ((isX3d(flags)) && _x3dValue) {
                RET_ONERROR( mywritestr(f, " ") )
                RET_ONERROR( _x3dValue->write(f, 0) )
            } else if (_value) {
                RET_ONERROR( mywritestr(f, " ") )
                RET_ONERROR( _value->write(f, 0) )
            }
        } else {
            RET_ONERROR( mywritestr(f, "\n") )
            TheApp->incSelectionLinenumber();
        }
    }
    return 0;
}

void Field::addToNodeType(int nodeType)
{
    _nodeType += nodeType;
    ExposedField *exposedField = getExposedField();
    if (exposedField) 
        exposedField->addToNodeType(nodeType);
}
