/*
 * NodeTwoSidedMaterial.h
 *
 * Copyright (C) 2009 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _NODE_TWO_SIDED_MATERIAL_H
#define _NODE_TWO_SIDED_MATERIAL_H

#ifndef _NODE_H
#include "Node.h"
#endif
#ifndef _PROTO_MACROS_H
#include "ProtoMacros.h"
#endif
#ifndef _PROTO_H
#include "Proto.h"
#endif

#include "SFMFTypes.h"

class ProtoTwoSidedMaterial : public Proto {
public:
                    ProtoTwoSidedMaterial(Scene *scene);
    virtual Node   *create(Scene *scene);

    virtual int     getType() const { return X3D_TWO_SIDED_MATERIAL; }
    virtual int     getNodeClass() const { return MATERIAL_NODE; }

    FieldIndex ambientIntensity;
    FieldIndex backAmbientIntensity;
    FieldIndex backDiffuseColor;
    FieldIndex backEmissiveColor;
    FieldIndex backShininess;
    FieldIndex backSpecularColor;
    FieldIndex backTransparency;
    FieldIndex diffuseColor;
    FieldIndex emissiveColor;
    FieldIndex shininess;
    FieldIndex separateBackColor;
    FieldIndex specularColor;
    FieldIndex transparency;
};

class NodeTwoSidedMaterial : public Node {
public:
                    NodeTwoSidedMaterial(Scene *scene, Proto *proto);

    virtual const char* getComponentName(void) const { return "Shape"; }
    virtual int         getComponentLevel(void) const { return 4; }
    virtual Node   *copy() const { return new NodeTwoSidedMaterial(*this); }

    fieldMacros(SFFloat, ambientIntensity,     ProtoTwoSidedMaterial);
    fieldMacros(SFFloat, backAmbientIntensity, ProtoTwoSidedMaterial);
    fieldMacros(SFColor, backDiffuseColor,     ProtoTwoSidedMaterial);
    fieldMacros(SFColor, backEmissiveColor,    ProtoTwoSidedMaterial);
    fieldMacros(SFFloat, backShininess,        ProtoTwoSidedMaterial);
    fieldMacros(SFColor, backSpecularColor,    ProtoTwoSidedMaterial);
    fieldMacros(SFFloat, backTransparency,     ProtoTwoSidedMaterial);
    fieldMacros(SFColor, diffuseColor,         ProtoTwoSidedMaterial);
    fieldMacros(SFColor, emissiveColor,        ProtoTwoSidedMaterial);
    fieldMacros(SFFloat, shininess,            ProtoTwoSidedMaterial);
    fieldMacros(SFBool,  separateBackColor,    ProtoTwoSidedMaterial);
    fieldMacros(SFColor, specularColor,        ProtoTwoSidedMaterial);
    fieldMacros(SFFloat, transparency,         ProtoTwoSidedMaterial);
};

#endif
