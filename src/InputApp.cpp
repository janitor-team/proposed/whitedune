/*
 * InputApp.cpp
 *
 * Copyright (C) 2007 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <errno.h>
#include "stdafx.h"
#include <math.h>

#include "DuneApp.h"
#include "resource.h"
#include "InputApp.h"
#include "PreferencesApp.h"

#ifndef HAVE_X3D_TO_X3DV_COMMAND
# define HAVE_X3D_TO_X3DV_COMMAND ""
#endif

InputApp::InputApp()
   {
   _x3d2X3dvCommand = NULL;
   _useInternalXmlParser = false;
   _x3dSize = -1;
   }

void
InputApp::InputSetDefaults()
   {
   if (_x3d2X3dvCommand != NULL)
       free(_x3d2X3dvCommand);
   _x3d2X3dvCommand = strdup(HAVE_X3D_TO_X3DV_COMMAND);
   _useInternalXmlParser = false;
   }

void
InputApp::InputLoadPreferences()
   {
   assert(TheApp != NULL);

   InputSetDefaults();

   if (_x3d2X3dvCommand != NULL)
       free(_x3d2X3dvCommand);
   _x3d2X3dvCommand = strdup(TheApp->GetPreference("X3d2X3dvCommand",
                                                   HAVE_X3D_TO_X3DV_COMMAND));
   _useInternalXmlParser =  TheApp->GetBoolPreference("UseInternalXmlParser", 
                                                      false);
   }

void
InputApp::InputSavePreferences()
   {
   assert(TheApp != NULL);

   if (_x3d2X3dvCommand != NULL)
       TheApp->SetPreference("X3d2X3dvCommand", _x3d2X3dvCommand);
   TheApp->SetBoolPreference("UseInternalXmlParser", _useInternalXmlParser);
   }

