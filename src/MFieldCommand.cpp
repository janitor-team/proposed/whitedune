/*
 * MFieldCommand.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "stdafx.h"

#include "MFieldCommand.h"
#include "Scene.h"

#include "Node.h"
#include "FieldValue.h"

MFieldCommand::MFieldCommand(Node *node, int field, int index, 
                             FieldValue *newValue)
{
    _node = node;
    _field = field;
    _index = index;
    if (newValue) {
        _newValue = newValue;
        _newValue->ref();
    } else {
        _newValue = ((MFieldValue *) node->getField(field))->getSFValue(index);
        _newValue->ref();
    }
    _oldValue = ((MFieldValue *) node->getField(field))->getSFValue(index);
    _oldValue->ref();
}

MFieldCommand::~MFieldCommand()
{
    _newValue->unref();
    _oldValue->unref();
}

void
MFieldCommand::execute()
{
    MFieldValue *value = (MFieldValue *) _node->getField(_field);

    _oldValue->unref();
    _oldValue = value->getSFValue(_index);
    _oldValue->ref();
    value->setSFValue(_index, _newValue);
    _node->getScene()->OnFieldChange(_node, _field, _index);
}

void
MFieldCommand::undo()
{
    MFieldValue *value = (MFieldValue *) _node->getField(_field);

    _newValue->unref();
    _newValue = value->getSFValue(_index);
    _newValue->ref();
    value->setSFValue(_index, _oldValue);
    _node->getScene()->OnFieldChange(_node, _field, _index);
}
