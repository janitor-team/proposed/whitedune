/*
 * NodeHAnimJoint.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "NodeHAnimJoint.h"
#include "Proto.h"
#include "FieldValue.h"
#include "MFString.h"
#include "SFString.h"
#include "SFInt32.h"
#include "MFInt32.h"
#include "SFFloat.h"
#include "MFFloat.h"
#include "SFVec3f.h"

ProtoHAnimJoint::ProtoHAnimJoint(Scene *scene)
  : ProtoTransform(scene, "HAnimJoint", HANIM_CHILD_NODE)
{
    displacers.set(
          addExposedField(MFNODE, "displacers", new MFNode(), 
                          X3D_HANIM_DISPLACER));
    limitOrientation.set(
          addExposedField(SFROTATION, "limitOrientation", 
                          new SFRotation(0.0f, 0.0f, 1.0f, 0.0f)));
    llimit.set(
          addExposedField(MFFLOAT, "llimit", new MFFloat()));
    name.set(
          addExposedField(SFSTRING, "name", new SFString()));
    skinCoordIndex.set(
          addExposedField(MFINT32, "skinCoordIndex", new MFInt32()));
    skinCoordWeight.set(
          addExposedField(MFFLOAT, "skinCoordWeight", new MFFloat()));
    stiffness.set(
          addExposedField(MFFLOAT, "stiffness", new MFFloat(),
                          new SFFloat(0.0f), new SFFloat(1.0f)));
    ulimit.set(
          addExposedField(MFFLOAT, "ulimit", new MFFloat()));
}

Node *
ProtoHAnimJoint::create(Scene *scene)
{ 
    return new NodeHAnimJoint(scene, this); 
}

NodeHAnimJoint::NodeHAnimJoint(Scene *scene, Proto *def)
  : NodeTransform(scene, def)
{
}

int
NodeHAnimJoint::getComponentLevel(void) const
{
    return 1;
}

const char* 
NodeHAnimJoint::getComponentName(void) const
{
    static const char* name = "H-Anim";
    return name;
}

