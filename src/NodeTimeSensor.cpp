/*
 * NodeTimeSensor.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "NodeTimeSensor.h"
#include "Proto.h"
#include "Scene.h"
#include "FieldValue.h"
#include "SFTime.h"
#include "SFBool.h"
#include "SFFloat.h"
#include "Field.h"
#include "ExposedField.h"


ProtoTimeSensor::ProtoTimeSensor(Scene *scene)
  : Proto(scene, "TimeSensor")
{
    cycleInterval.set(
          addExposedField(SFTIME, "cycleInterval", new SFTime(1.0), 
                          new SFTime(0.0)));
    enabled.set(
          addExposedField(SFBOOL, "enabled", new SFBool(true)));
    loop.set(
          addExposedField(SFBOOL, "loop", new SFBool(false)));

    pauseTime.set(
          addExposedField(SFTIME, "pauseTime", new SFTime(0.0)));
    setFieldFlags(pauseTime, FF_X3D_ONLY);
    resumeTime.set(
          addExposedField(SFTIME, "resumeTime", new SFTime(0.0))); 
    setFieldFlags(resumeTime, FF_X3D_ONLY);
    startTime.set(
          addExposedField(SFTIME, "startTime", new SFTime(0.0f)));
    setFieldFlags(startTime, EIF_RECOMMENDED);
    stopTime.set(
          addExposedField(SFTIME, "stopTime", new SFTime(0.0f)));
    setFieldFlags(stopTime, EIF_RECOMMENDED);
    addEventOut(SFTIME, "cycleTime");
    addEventOut(SFTIME, "elapsedTime", FF_X3D_ONLY);
    fraction_changed.set(addEventOut(SFFLOAT, "fraction_changed", 
                         EOF_RECOMMENDED));
    addEventOut(SFBOOL, "isActive");
    addEventOut(SFBOOL, "isPaused", FF_X3D_ONLY);
    time.set(addEventOut(SFTIME, "time"));
}

Node *
ProtoTimeSensor::create(Scene *scene)
{ 
    return new NodeTimeSensor(scene, this); 
}

NodeTimeSensor::NodeTimeSensor(Scene *scene, Proto *def)
  : Node(scene, def)
{
    _active = true;
    _startTime = 0;
    _stopTime = 0;
}

void
NodeTimeSensor::preDraw()
{
    if (enabled()->getValue()) _scene->addTimeSensor(this);
}

void
NodeTimeSensor::setTime(double t)
{
    double dstopTime = _stopTime;

    if (enabled()->getValue()) {
        if ((t > _startTime) && (t < dstopTime)) {
            _active = true;
        }
        if (_active) {
            if ((t >= dstopTime) && 
                (dstopTime > _startTime) && !loop()->getValue()) {
                _active = false;
            }
            double temp = (t - _startTime) / cycleInterval()->getValue();
            double fraction = temp - floor(temp);
            if (fraction == 0.0 && (t > _startTime)) fraction = 1.0;
            sendEvent(fraction_changed_Field(), t, 
                      new SFFloat((float) fraction));
            sendEvent(time_Field(), t, new SFTime(t));
        }
    }
}

void
NodeTimeSensor::checkStart(bool loop, double startTime, double stopTime, 
                           double t)
{
    if ((t < _stopTime) || ((stopTime <= startTime) && loop)) {
        _active = true;
        _startTime = t;
        _stopTime = stopTime;
    } else {
        _active = false;
        _startTime = startTime;
        _stopTime = stopTime;
    }
}

void
NodeTimeSensor::start(double t)
{
    checkStart(loop()->getValue(), startTime()->getValue(), 
               stopTime()->getValue(), t);
}

void    
NodeTimeSensor::updateStart(int field, FieldValue *value, double t)
{
    checkStart(loop()->getValue(), startTime()->getValue(), 
               stopTime()->getValue(), t);
}

int
NodeTimeSensor::getProfile(void) const
{ 
    return PROFILE_INTERCHANGE;
}


int
NodeTimeSensor::getComponentLevel(void) const
{
    if (hasOutput("isPaused"))
        return 2;    
    if (!isDefault(pauseTime_Field()))
        return 2;    
    if (!isDefault(resumeTime_Field()))
        return 2;    
    return -1;
}

const char* 
NodeTimeSensor::getComponentName(void) const
{
    static const char* name = "Time";
    return name;
}





