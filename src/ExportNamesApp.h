/*
 * ExportNamesApp.h
 *
 * Copyright (C) 2009 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _EXPORT_NAMES_APP_H
#define _EXPORT_NAMES_APP_H 1

#ifndef _DUNE_STRING_H
# include "MyString.h"
#endif

class ExportNamesApp {
public:
                        ExportNamesApp();

    void                setPrefix(const char* prefix);
    const char         *getPrefix(void) { return _prefix; }

    void                setCPrefix(const char* prefix);
    const char         *getCPrefix(void);

    void                setAppendPrefix(const char* prefix);
    void                updatePrefix(void);
    bool                isFirstCExport(void) { return _appendPrefix == NULL; }

    void                buildCSceneGraphName(void);
    const char         *getCSceneGraphName(void) { return _cSceneGraphName; }

    void                buildCNodeName(void);
    const char         *getCNodeName(void) { return _cNodeName; }
    const char         *getCNodeNamePtr(void) { return _cNodeNamePtr; }
    const char         *getCNodeNameArray(void) { return _cNodeNameArray; }

protected:
    const char         *_prefix;
    const char         *_appendPrefix;
    MyString            _cNodeName;
    MyString            _cNodeNamePtr;
    MyString            _cNodeNameArray;
    MyString            _cSceneGraphName;
};

#endif
