/*
 * WonderlandModuleExportDialog.cpp
 *
 * Copyright (C) 1999 Stephen F. White/2010 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <ctype.h>
#include "stdafx.h"
#include "WonderlandModuleExportDialog.h"
#include "DuneApp.h"
#include "resource.h"
#include "swt.h"

WonderlandModuleExportDialog::WonderlandModuleExportDialog(SWND parent)
  : Dialog(parent, IDD_WONDERLAND_MODULE_EXPORT)
{
    LoadData();
}

void
WonderlandModuleExportDialog::OnCommand(int id)
{
    if (id == IDOK) {
        SaveData();
        if (Validate()) {
            swEndDialog(IDOK);
        }
    } else if (id == IDC_BROWSE_WONDERLAND_EXPORT_PATH) {
        char buf[1024];
        strcpy(buf, "modules");
        const char *choice = "modules directory (modules)\0modules;\0\0";
        if (swOpenDirDialog(TheApp->mainWnd(), "Enter modules directory", 
                            choice, buf, 1023)) {
            TheApp->SetWonderlandModuleExportPath(buf);
            LoadData();
        }
    } else if (id == IDCANCEL) {
        swEndDialog(IDCANCEL);
    }
}

void WonderlandModuleExportDialog::LoadData()
{
    swSetText(swGetDialogItem(_dlg, IDC_WONDERLAND_MODULE_EXPORT_PATH), 
                              TheApp->GetWonderlandModuleExportPath());
    swSetText(swGetDialogItem(_dlg, IDC_WONDERLAND_MODULE_EXPORT_PREFIX), 
                              TheApp->GetWonderlandModuleExportPrefix());
    swSetCheck(swGetDialogItem(_dlg, IDC_WONDERLAND_MODULE_EXPORT_MANY_CLASSES),
                               TheApp->GetWonderlandModuleExportManyClasses());
}

bool
WonderlandModuleExportDialog::Validate()
{
    const char *prefix = TheApp->GetWonderlandModuleExportPrefix();
    bool invalidPrefix = false;
    if ((prefix == NULL) || (strlen(prefix) == 0))
        invalidPrefix = true;
    else
       for (int i = 0; i < strlen(prefix); i++)
           if ((!isascii(prefix[i])) || (!isalnum(prefix[i])))
               invalidPrefix = true;
    if (invalidPrefix) {
        TheApp->MessageBoxId(IDS_NEED_WONDERLAND_MODULE_PREFIX);
        return false;
    }
    if (TheApp->GetWonderlandModuleExportPath() == NULL)
        return false;
    if (strlen(TheApp->GetWonderlandModuleExportPath()) == 0)
        return false;
    return true;
}


void
WonderlandModuleExportDialog::SaveData() 
{
    char temp[1024];

    swGetText(swGetDialogItem(_dlg, IDC_WONDERLAND_MODULE_EXPORT_PATH), temp, 
                              1023);
    TheApp->SetWonderlandModuleExportPath(temp);
    swGetText(swGetDialogItem(_dlg, IDC_WONDERLAND_MODULE_EXPORT_PREFIX), temp, 
                              1023);
    TheApp->SetWonderlandModuleExportPrefix(temp);
    TheApp->SetWonderlandModuleExportManyClasses(swGetCheck(swGetDialogItem(
           _dlg, IDC_WONDERLAND_MODULE_EXPORT_MANY_CLASSES)));
}
