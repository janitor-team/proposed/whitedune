/*
 * NodeGeoLocation.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "NodeGeoLocation.h"
#include "Proto.h"
#include "MFVec3f.h"
#include "ExposedField.h"
#include "Field.h"
#include "RenderState.h"
#include "DuneApp.h"
#include "Util.h"
#include "Vec3f.h"
#include "Scene.h"
#include "NodeIndexedFaceSet.h"
#include "NodeIndexedLineSet.h"
#include "NodePointSet.h"

ProtoGeoLocation::ProtoGeoLocation(Scene *scene)
  : GeoProto(scene, "GeoLocation")
{
    addEventIn(MFNODE, "addChildren");
    addEventIn(MFNODE, "removeChildren");
    children.set(
          addExposedField(MFNODE, "children", new MFNode(), CHILD_NODE));

    geoCoords.set(
          addExposedField(SFSTRING, "geoCoords", new SFString()));
    setFieldFlags(geoCoords, FF_VRML_ONLY);
    geoCoordsX3D.set(
          addExposedField(SFVEC3D, "geoCoords", new SFVec3d()));
    setFieldFlags(geoCoordsX3D, FF_X3D_ONLY);

    bboxCenter.set(
          addField(SFVEC3F, "bboxCenter", new SFVec3f(0.0f, 0.0f, 0.0f)));
    bboxSize.set(
          addField(SFVEC3F, "bboxSize", new SFVec3f(-1.0f, -1.0f, -1.0f), 
                   new SFFloat(-1.0f)));
}

Node *
ProtoGeoLocation::create(Scene *scene)
{ 
    return new NodeGeoLocation(scene, this); 
}

NodeGeoLocation::NodeGeoLocation(Scene *scene, Proto *def)
  : GeoNode(scene, def)
{
}

void
NodeGeoLocation::setField(int index, FieldValue *value)
{
    if (index == geoCoords_Field()) {
        SFVec3d *value3d = new SFVec3d((SFString *)value);
        Node::setField(geoCoordsX3D_Field(), value3d);
    }
    Node::setField(index, value);
    update();
}

Node *
NodeGeoLocation::convert2Vrml(void) 
{
    const double *values = geoCoordsX3D()->getValue();
    char buf[4096];
    mysnprintf(buf, 4095, "%g %g %g", values[0], values[1], values[2]);
    SFString *string = new SFString(buf);
    geoCoords(string);    
    return NULL;
}



