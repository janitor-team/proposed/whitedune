/*
 * MeshBasedNode.h
 *
 * Copyright (C) 1999 Stephen F. White, 2004 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _MESH_BASED_NODE_H
#define _MESH_BASED_NODE_H

#ifndef GEOMETRY_NODE_H
#include "GeometryNode.h"
#endif
#ifndef _VEC3F
# include "Vec3f.h"
#endif

#include "MeshFlags.h"

class Scene;
class MFVec3f;
class MFVec2f;
class MFInt32;
class Mesh;
class Face;

class MeshBasedNode : public GeometryNode {
public:
                    MeshBasedNode(Scene *scene, Proto *proto);
    virtual        ~MeshBasedNode();

    int             optimizeNormals(int *coordIndex, Vec3f *vertices,
                                    Vec3f *normals, int index, bool ccw, 
                                    bool cleanDoubleVertices = true);
    int             optimizeNormals(int *coordIndex, MFVec3f *vertices,
                                    MFVec3f *normals, int index, bool ccw, 
                                    bool cleanDoubleVertices = true);
    virtual void    meshDraw();
    virtual void    drawNormals();

    virtual Node   *toIndexedFaceSet(int meshFlags = MESH_WANT_NORMAL,
                                     bool cleanVertices = true);
    virtual bool    canConvertToIndexedFaceSet(void) { return true; }

    virtual Node   *toIndexedTriangleSet(int meshFlags = MESH_TARGET_HAS_CCW);

    virtual bool    canConvertToIndexedTriangleSet(void) { return true; }
    // currently, the optimization of the convert to IndexedTriangleSet 
    // would break normals, color and TextureCoordinates
    virtual bool    shouldConvertToIndexedTriangleSet(void) { return false; }
    virtual bool    optimizeOnConvertionToIndexedTriangleSet(void) 
                       { return true; }

    virtual Node   *alreadyTriangulatedToIndexedTriangleSet(bool hasCcw = true);


    virtual Node   *toTriangleSet(int meshFlags = MESH_TARGET_HAS_CCW);
    virtual bool    canConvertToTriangleSet(void) 
#ifdef HAVE_GLUNEWTESS
       { return true; }
#else
       { return false; }
#endif

    virtual bool    isInvalidChildNode(void) { return true; }
    virtual bool    hasBoundingBox(void) { return true; }
    virtual Vec3f   getMinBoundingBox(void);
    virtual Vec3f   getMaxBoundingBox(void);

    virtual void    setTexCoordFromMesh(Node *ntexCoord);

    void            update(void) { _meshDirty = true; }
    void            reInit(void) { _mesh = NULL; _meshDirty = true; }

    virtual void    flip(int index);
    virtual void    swap(int fromTo);

    virtual int     countPolygons(void);
    virtual int     countPolygons1Sided(void);
    virtual int     countPolygons2Sided(void);

    virtual int     write(int filedes, int indent);
    virtual int     writeXml(int filedes, int indent);

    virtual Node   *getConvertedNode(void);

    virtual bool    canWriteAc3d() { return true; }
    virtual int     writeAc3d(int filedes, int indent);

    virtual bool    canWriteCattGeo() { return true; }
    virtual int     writeCattGeo(int filedes, int indent);

    virtual bool    canWriteLdrawDat() { return true; }
    virtual int     writeLdrawDat(int filedes, int indent);

    void            setCreaseAngle(float angle);

    MFVec3f        *getSmoothNormals(void);
    MFInt32        *getSmoothNormalIndex(void);

    MFVec3f        *getVertices(void);
    MFVec2f        *getTextureCoordinates();
    MFInt32        *getTexCoordIndex(); 

    virtual void    createMesh(bool cleanDoubleVertices = true) = 0;

    void            smoothNormals(bool optimizeNormals);
    virtual bool    isFlat(void);

    void            optimizeMesh(MFVec3f *newCoord, MFInt32 *newCoordIndex);
    Mesh           *triangulateMesh(void);
    MFInt32        *getCoordIndexFromMesh(void);
    MFVec3f        *getCoordFromMesh(void);

    bool            meshDirty(void) { return (_mesh == NULL) || _meshDirty; }
    
protected:
    Mesh           *_mesh;
    bool            _meshDirty;
};

#endif // _MESH_BASED_NODE_H

