/*
 * PreviewSettingsDialog.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "stdafx.h"
#include "PreviewSettingsDialog.h"
#include "DuneApp.h"
#include "resource.h"
#include "swt.h"
#include "WriteFlags.h"

PreviewSettingsDialog::PreviewSettingsDialog(SWND parent)
  : Dialog(parent, IDD_PREVIEW_SETTINGS)
{
    LoadData();
}

void
PreviewSettingsDialog::OnCommand(int id)
{
    switch(id) {
      case IDC_BROWSE:
        OnBrowse();
        break;
      case IDC_USE_REMOTE:
        OnUseRemote();
        break;
      case IDC_BROWSER_USE_DEFAULT:
        OnSetDefault();
        break;
      default:
        Dialog::OnCommand(id);
        break;
    }
}

static MyString
GetText(SWND item)
{
    char buf[1024];
    swGetText(item, buf, 1024);
    return buf;
}

static const char *vrmlLevelStrings[] = { 
    "pure VRML97",
    "VRML97 Amendment 1",
    "VRML97 with COVER extensions"
    "pure X3DV",
    "X3DV"
#ifdef HAVE_XJ3D_RIGID_BODY_PHYSICS_COMPONENT
    ,"X3DV with rigid body XJ3D extensions"
#endif
};

void PreviewSettingsDialog::LoadData()
{
    const char *command;
    int vrmlLevel;
    int useRemote;
    int xtypExecuteOrFork;
    const char *remoteCommand;
    const char *application;
    const char *topic;

    swBrowserGetSettings(TheApp->GetBrowser(), &command, 
                         &vrmlLevel, &useRemote, &xtypExecuteOrFork,
                         &remoteCommand, &application, &topic);
    swSetText(swGetDialogItem(_dlg, IDC_COMMAND), command);

    SWND comboVrmlLevel = swGetDialogItem(_dlg, IDC_BROWSER_VRML_LEVEL);
    swComboBoxDeleteAll(comboVrmlLevel);
    for (int i=0;i<(sizeof(vrmlLevelStrings)/sizeof(const char*));i++)
        swComboBoxAppendItem(comboVrmlLevel, vrmlLevelStrings[i]);
    int comboSelection = 1;
    switch (vrmlLevel) {
      case PURE_VRML97:
        comboSelection = 0;
        break;
      case COVER:
        comboSelection = 2;
        break;
      case PURE_X3DV:
        comboSelection = 3;
        break;
      case X3DV:
        comboSelection = 4;
        break;
#ifdef HAVE_XJ3D_RIGID_BODY_PHYSICS_COMPONENT
      case XJ3D:
        comboSelection = 5;
        break;
#endif
    }
    swComboBoxSetSelection(comboVrmlLevel, comboSelection);

    swSetCheck(swGetDialogItem(_dlg, IDC_USE_REMOTE), useRemote);
#ifdef _WIN32
    swSetCheck(swGetDialogItem(_dlg, IDC_XTYP_EXECUTE), xtypExecuteOrFork);
#else
    swSetCheck(swGetDialogItem(_dlg, IDC_FORK_ON_PREVIEW), xtypExecuteOrFork);
    swSetCheck(swGetDialogItem(_dlg, IDC_PREVIEW_CONSOLE), 
               swBrowserGetSettingsErrorToConsole(TheApp->GetBrowser()));
#endif
    swSetText(swGetDialogItem(_dlg, IDC_REMOTE_COMMAND), remoteCommand);
    swSetText(swGetDialogItem(_dlg, IDC_APPLICATION), application);
    swSetText(swGetDialogItem(_dlg, IDC_TOPIC), topic);
}

void
PreviewSettingsDialog::SaveData()
{
    MyString command;
    int vrmlLevel;
    int useRemote;
    int xtypExecuteOrFork;
    MyString remoteCommand;
    MyString application;
    MyString topic;

    command = GetText(swGetDialogItem(_dlg, IDC_COMMAND));
    useRemote = swGetCheck(swGetDialogItem(_dlg, IDC_USE_REMOTE));
    OnUseRemote();
    int comboSelection = swComboBoxGetSelection(swGetDialogItem(_dlg,
                                                IDC_BROWSER_VRML_LEVEL));
    switch (comboSelection) {
      case 0:
        vrmlLevel = PURE_VRML97;
        break;
      case 1:
        vrmlLevel = 0;
        break;    
      case 2:
        vrmlLevel = COVER;
        break;
      case 3:
        vrmlLevel = PURE_X3DV;
        break;
      case 4:
        vrmlLevel = X3DV;
        break;
#ifdef HAVE_XJ3D_RIGID_BODY_PHYSICS_COMPONENT
      case 5:
        vrmlLevel = XJ3D;
        break;
#endif
    }
#ifdef _WIN32
    xtypExecuteOrFork = swGetCheck(swGetDialogItem(_dlg, IDC_XTYP_EXECUTE));
#else
    xtypExecuteOrFork = swGetCheck(swGetDialogItem(_dlg, IDC_FORK_ON_PREVIEW));
    int errorToConsole = swGetCheck(swGetDialogItem(_dlg, IDC_PREVIEW_CONSOLE));
    swBrowserSetSettingsErrorToConsole(TheApp->GetBrowser(), errorToConsole);
#endif
    remoteCommand = GetText(swGetDialogItem(_dlg, IDC_REMOTE_COMMAND));
    application = GetText(swGetDialogItem(_dlg, IDC_APPLICATION));
    topic = GetText(swGetDialogItem(_dlg, IDC_TOPIC));
    swBrowserSetSettings(TheApp->GetBrowser(), command, vrmlLevel, useRemote,
                         xtypExecuteOrFork, remoteCommand, application, topic);
}

void PreviewSettingsDialog::OnBrowse() 
{
    char buf[1024] = "";
    char buf2[1024] = "";

    if (TheApp->browseCommand(buf, 1024, IDS_PREVIEW_COMMAND_PROMPT)) {
        mysnprintf(buf2, 1024, "%s %%s", buf);
        swSetText(swGetDialogItem(_dlg, IDC_COMMAND), buf2);
    }
}

void
PreviewSettingsDialog::OnUseRemote()
{
    int flag = swGetCheck(swGetDialogItem(_dlg, IDC_USE_REMOTE));

#ifdef _WIN32
    swEnableWindow(swGetDialogItem(_dlg, IDC_REMOTE_COMMAND), flag);
    swEnableWindow(swGetDialogItem(_dlg, IDC_APPLICATION), flag);
    swEnableWindow(swGetDialogItem(_dlg, IDC_TOPIC), flag);
#endif
}

void
PreviewSettingsDialog::OnSetDefault()
{
    swBrowserSetDefault(TheApp->GetBrowser());    
    LoadData();
}

