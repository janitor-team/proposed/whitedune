/*
 * AnimationDialog.h
 *
 * Copyright (C) 2003 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _ANIMATION_DIALOG_H
#define _ANIMATION_DIALOG_H

#include "Scene.h"
#include "CheckBoxWindow.h"

class NodeTimeSensor;

class AnimationDialog : public Dialog {
public:
                        AnimationDialog(SWND parent, Node* oldNode);

    NodeTimeSensor     *getTimeSensor()        { return _timeSensor; }
    float               getTimeSensorSeconds() { return _newTimeSensorSeconds; }
    Array<int>         &getEventInFields()     { return _eventInFields; }    
    Array<int>         &getEventInTypes()      { return _eventInTypes; }    
    Array<bool>        &getEventInIsAnimated() { return _eventInIsAnimated; }
    void                buildInterfaceData(void);

protected:
    virtual SWND        dialog(void) { return _dlg; }
    void                LoadData();
    void                SaveData();
    virtual bool        Validate();
    int                 numEventInNames() { return _eventInNames.size(); }
    
protected:
    CheckBoxWindow      _window;
    Node               *_animationNode;
    NodeTimeSensor     *_timeSensor;
    float               _newTimeSensorSeconds;
    bool                _okFlag;
    Array<MyString>     _eventInNames;
    Array<int>          _eventInFields;
    Array<int>          _eventInTypes;
    Array<bool>         _eventInIsAnimated;
};

#endif

