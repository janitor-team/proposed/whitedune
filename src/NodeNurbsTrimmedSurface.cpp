/*
 * NodeNurbsTrimmedSurface.cpp
 *
 * Copyright (C) 1999 Stephen F. White, 2008 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "stdafx.h"

#include "NodeNurbsTrimmedSurface.h"
#include "Proto.h"
#include "FieldValue.h"
#include "MFNode.h"
#include "SFVec3f.h"
#include "SFFloat.h"
#include "Scene.h"
#include "NodeTrimmedSurface.h"
#include "MoveCommand.h"

ProtoNurbsTrimmedSurface::ProtoNurbsTrimmedSurface(Scene *scene)
  : ProtoNurbsSurface(scene, "NurbsTrimmedSurface")
{                   
    addEventIn(MFNODE, "addTrimmingContour");
    addEventIn(MFNODE, "removeTrimmingContour");
    trimmingContour.set(
          addExposedField(MFNODE, "trimmingContour", new MFNode(), 
                          VRML_CONTOUR_2D));
}

Node *
ProtoNurbsTrimmedSurface::create(Scene *scene)
{ 
    return new NodeNurbsTrimmedSurface(scene, this); 
}

NodeNurbsTrimmedSurface::NodeNurbsTrimmedSurface(Scene *scene, Proto *def)
  : NodeNurbsSurface(scene, def)
{
}

Node *
NodeNurbsTrimmedSurface::toTrimmedSurface(void)
{
    NodeTrimmedSurface *ret = (NodeTrimmedSurface *)
                              _scene->createNode("TrimmedSurface");
    NodeNurbsSurface *nsurface = (NodeNurbsSurface *)
                                 ((NodeNurbsSurface *)this)->copy();

    nsurface->NodeNurbsSurface::convert2Vrml();
    
    _scene->execute(new MoveCommand(nsurface, NULL, -1, ret,
                                    ret->surface_Field()));


    _scene->changeRoutes(this, this->controlPoint_Field(), nsurface, 
                         nsurface->controlPoint_Field(), false);        
    return ret;
}

Node *
NodeNurbsTrimmedSurface::convert2Vrml(void)
{
    NodeTrimmedSurface *ret = (NodeTrimmedSurface *)toTrimmedSurface();

    NodeList *trimm = trimmingContour()->getValues();
    for (int i = 0; i < trimm->size(); i++)
        _scene->execute(new MoveCommand(trimm->get(i), NULL, -1, ret, 
                                        ret->trimmingContour_Field()));
    return ret;    
}

