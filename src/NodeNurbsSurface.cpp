/*
 * NodeNurbsSurface.cpp
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdlib.h>
#include <stdio.h>

#include "stdafx.h"
#include "NodeNurbsSurface.h"
#include "Scene.h"
#include "FieldValue.h"
#include "SFInt32.h"
#include "Mesh.h"
#include "MFFloat.h"
#include "MFInt32.h"
#include "MFVec2f.h"
#include "MFVec3f.h"
#include "SFNode.h"
#include "SFBool.h"
#include "SFVec3f.h"
#include "Vec2f.h"
#include "Vec3f.h"
#include "RenderState.h"
#include "DuneApp.h"
#include "NodeCoordinate.h"
#include "NodeNormal.h"
#include "NodeTextureCoordinate.h"
#include "NodeIndexedFaceSet.h"
#include "NodeNurbsGroup.h"
#include "NodeNurbsSet.h"
#include "NurbsSurfaceDegreeElevate.h"
#include "Util.h"
#include "Field.h"
#include "ExposedField.h"
#include "MoveCommand.h"
#include "NodeNurbsTrimmedSurface.h"
#include "resource.h"

ProtoNurbsSurface::ProtoNurbsSurface(Scene *scene)
  : Proto(scene, "NurbsSurface")
{
    addElements(); 
}

ProtoNurbsSurface::ProtoNurbsSurface(Scene *scene, const char *name)
  : Proto(scene, name)
{
    addElements(); 
}

void ProtoNurbsSurface::addElements(void) 
{
    uDimension.set(
          addField(SFINT32, "uDimension", new SFInt32(0), new SFInt32(0)));
    vDimension.set(
          addField(SFINT32, "vDimension", new SFInt32(0), new SFInt32(0)));
    uKnot.set(
          addField(MFFLOAT, "uKnot", new MFFloat()));
    vKnot.set(
          addField(MFFLOAT, "vKnot", new MFFloat()));
    uOrder.set(
          addField(SFINT32, "uOrder", new SFInt32(3), new SFInt32(2)));
    vOrder.set(
          addField(SFINT32, "vOrder", new SFInt32(3), new SFInt32(2)));
    controlPoint.set(
          addExposedField(MFVEC3F, "controlPoint", new MFVec3f()));
    setFieldFlags(controlPoint, FF_VRML_ONLY | EIF_RECOMMENDED);
    controlPointX3D.set(
          addExposedField(SFNODE, "controlPoint", new SFNode(NULL), 
                          COORDINATE_NODE));
    setFieldFlags(controlPointX3D, FF_X3D_ONLY);
    weight.set(
          addExposedField(MFFLOAT, "weight", new MFFloat(), new SFFloat(0.0f)));
    uTessellation.set(
          addExposedField(SFINT32, "uTessellation", new SFInt32(0)));
    vTessellation.set(
          addExposedField(SFINT32, "vTessellation", new SFInt32(0)));
    texCoord.set(
          addExposedField(SFNODE, "texCoord", new SFNode(NULL), 
                          NURBS_TEXTURE_COORDINATE_OR_TEXTURE_COORDINATE_NODE));
    ccw.set(
          addField(SFBOOL, "ccw", new SFBool(true)));
    setFieldFlags(ccw, FF_VRML_ONLY);
    uClosed.set(
          addField(SFBOOL, "uClosed", new SFBool(false)));
    setFieldFlags(uClosed, FF_X3D_ONLY);
    vClosed.set(
          addField(SFBOOL, "vClosed", new SFBool(false)));
    setFieldFlags(vClosed, FF_X3D_ONLY);
    solid.set(
          addField(SFBOOL, "solid", new SFBool(true)));

    _x3dName = "";
    _x3dName += "NurbsPatchSurface";
}

Node *
ProtoNurbsSurface::create(Scene *scene)
{
    return new NodeNurbsSurface(scene, this); 
}

NodeNurbsSurface::NodeNurbsSurface(Scene *scene, Proto *proto)
  : MeshMorphingNode(scene, proto)
{
    _x3d = scene->isX3d();
}

NodeNurbsSurface::~NodeNurbsSurface()
{
}

MFVec3f *
NodeNurbsSurface::getControlPoints(void)
{
    if (_x3d) {
        Node *pointNode = controlPointX3D()->getValue();
        if (pointNode)
            return ((NodeCoordinate *)pointNode)->point();
    }
    return controlPoint();
}

void
NodeNurbsSurface::setControlPoints(const MFVec3f *points)
{
    if (_x3d) {
        NodeCoordinate *coord = (NodeCoordinate *)controlPointX3D()->getValue();
        if (coord != NULL)
            _scene->setField(coord, coord->point_Field(), new MFVec3f(points));
    } else
        _scene->setField(this, controlPoint_Field(), new MFVec3f(points));
}

void
NodeNurbsSurface::createControlPoints(const MFVec3f *points)
{
    if (_x3d)
        controlPointX3D(new SFNode(_scene->createNode("Coordinate"))); 
    setControlPoints(points);
}

void
NodeNurbsSurface::repairNormal(MFVec3f *normals, MFVec3f *tess, 
                               int c1, int c2, int c3)
{
    Vec3f tessC1 = tess->getVec(c1); 
    Vec3f tessC2 = tess->getVec(c2); 
    Vec3f tessC3 = tess->getVec(c3); 
    Vec3f v1 = tessC3 - tessC1;
    Vec3f v2 = tessC2 - tessC1;
    Vec3f normal = ccw()->getValue() ? v2.cross(v1) : v1.cross(v2);
    normal.normalize();
    normals->setVec(c1, normal);   
}

void
NodeNurbsSurface::createMesh(const Vec3f *controlPoints, bool cleanVertices)
{
    int iuDimension = uDimension()->getValue();
    int ivDimension = vDimension()->getValue();
    int iuTess = uTessellation()->getValue();
    int ivTess = vTessellation()->getValue();
    float *weights = NULL;
    int i, j;

    if (iuDimension == 0 || ivDimension == 0) return;

    if (uKnot()->getSize() != uOrder()->getValue() + iuDimension
     || vKnot()->getSize() != vOrder()->getValue() + ivDimension)
        return;

    if (weight()->getSize() == 0) {
        weights = new float[iuDimension * ivDimension];
        for (i = 0; i < iuDimension * ivDimension; i++) {
            weights[i] = 1.0f;
        }
    } else if (weight()->getSize() != iuDimension * ivDimension) {
        return;
    }

    MFVec2f *texCoords = NULL;
    if (texCoord()->getValue()) 
        if (texCoord()->getValue()->getType() == VRML_TEXTURE_COORDINATE)
            texCoords = ((NodeTextureCoordinate *)(texCoord()->getValue()))
                         ->point();

    if (iuTess <= 0) iuTess = TheApp->getTessellation();
    if (ivTess <= 0) ivTess = TheApp->getTessellation();
    int size = (iuTess + 1 + iuTess % 2) * (ivTess + 1);
    MFVec3f *vertices = new MFVec3f(size);
    MFVec3f *normal = new MFVec3f(size);

    const float *uKnots = uKnot()->getValues();
    const float *vKnots = vKnot()->getValues();
    float uInc = (uKnots[uKnot()->getSize()-1] - uKnots[0]) / iuTess;
    float vInc = (vKnots[vKnot()->getSize()-1] - vKnots[0]) / ivTess;

    int index = 0;
    float u, v;
    const float *w = weights ? weights : weight()->getValues();
    for (j = 0, v = vKnots[0]; j <= ivTess; j++, v += vInc) {
        for (i = 0, u = uKnots[0]; i <= iuTess; i++, u += uInc) {
            Vec3f norm;
            Vec3f tess = surfacePoint(iuDimension, uOrder()->getValue(), uKnots,
                                      ivDimension, vOrder()->getValue(), vKnots,
                                      controlPoints, w, u, v, norm);
            vertices->setVec(index, tess);
            if (!ccw()->getValue()) norm = -norm;
            normal->setVec(index, norm);
            index++;
        }
    }

    index = 0;
    Vec2f *tc = new Vec2f[size];
    float uinv = 1.0f / iuTess;
    float vinv = 1.0f / ivTess;
    for (j = 0; j <= ivTess; j++) {
        for (i = 0; i <= iuTess; i++) {
            tc[index].x = i * uinv;
            tc[index].y = j * vinv;
            index++;
        }
    }
    int *ci = new int[size * 8];
    index = 0;
    bool midQuadUTess = -1;
    if ((iuTess % 2) == 1) 
        midQuadUTess = iuTess / 2;    
    for (j = 0; j < ivTess; j++) {
        for (i = 0; i < iuTess; i++) {
            int c1 = j * (iuTess+1) + i;
            int c2 = j * (iuTess+1) + (i+1);
            int c3 = (j+1) * (iuTess+1) + (i+1);
            int c4 = (j+1) * (iuTess+1) + i;

            // repair normals at begin of v
            if (j == 0) {
                repairNormal(normal, vertices, c1, c3, c4);
                repairNormal(normal, vertices, c2, c3, c4);
            }
            // repair normals at end of v
            if (j == (ivTess - 1)) {
                repairNormal(normal, vertices, c3, c1, c2);
                repairNormal(normal, vertices, c4, c1, c2);
            }

            Vec3f tessC1 = vertices->getVec(c1);
            Vec3f tessC2 = vertices->getVec(c2);
            Vec3f tessC3 = vertices->getVec(c3);
            Vec3f tessC4 = vertices->getVec(c4);
            if (i == midQuadUTess) {
                if (cleanVertices && !tessC1.validTriangle(tessC2, tessC3)) {
                    ci[index++] = c2;
                    ci[index++] = c3;
                    ci[index++] = c4;
                    ci[index++] = -1;                
                } else if (cleanVertices && 
                           !tessC3.validTriangle(tessC4, tessC1)) {
                    ci[index++] = c1;
                    ci[index++] = c2;
                    ci[index++] = c4;
                    ci[index++] = -1;
                } else {
                    ci[index++] = c1;
                    ci[index++] = c2;
                    ci[index++] = c3;
                    ci[index++] = c4;
                    ci[index++] = -1;
                }
            } else {
                if (i > iuTess / 2) {
                    if (!cleanVertices || 
                        tessC1.validTriangle(tessC2, tessC3)) {
                        ci[index++] = c1;
                        ci[index++] = c2;
                        ci[index++] = c3;
                        ci[index++] = -1;
                    }
                } else {
                    if (!cleanVertices || 
                        tessC4.validTriangle(tessC1, tessC2)) {
                        ci[index++] = c4;
                        ci[index++] = c1;
                        ci[index++] = c2;
                        ci[index++] = -1;
                    }
                }
                if (i > iuTess / 2) {
                    if (!cleanVertices || 
                        tessC3.validTriangle(tessC4, tessC1)) {
                        ci[index++] = c3;
                        ci[index++] = c4;
                        ci[index++] = c1;
                        ci[index++] = -1;
                    }
                } else {
                    if (!cleanVertices || 
                        tessC2.validTriangle(tessC3, tessC4)) {
                        ci[index++] = c2;
                        ci[index++] = c3;
                        ci[index++] = c4;
                        ci[index++] = -1;
                    }
                }
            }
        }
    }

    MFInt32 *coordIndex = new MFInt32(ci, index);
    if (!texCoords) texCoords = new MFVec2f((float *) tc, size * 2);

    int meshFlags = 0;
    if (ccw()->getValue())
        meshFlags |= MESH_CCW;
    if (solid()->getValue())
        meshFlags |= MESH_SOLID;
    meshFlags |= MESH_CONVEX;
    meshFlags |= MESH_COLOR_PER_VERTEX;
    meshFlags |= MESH_NORMAL_PER_VERTEX;
    if (_mesh)
        delete _mesh;
    _mesh = new Mesh(vertices, coordIndex, normal, NULL,
                     NULL, NULL, texCoords, NULL, 1.57, meshFlags);
    delete [] weights;
}

void
NodeNurbsSurface::createMesh(bool cleanVertices)
{
    if (getControlPoints()->getSize() != uDimension()->getValue() * 
                                     vDimension()->getValue() * 3)
        return;

    createMesh((const Vec3f *) getControlPoints()->getValues(), cleanVertices);
}

void
NodeNurbsSurface::drawHandles()
{
    int iuDimension = uDimension()->getValue();
    int ivDimension = vDimension()->getValue();
    RenderState state;

    if (getControlPoints()->getSize() != iuDimension * ivDimension * 3)
        return;

    if (weight()->getSize() != iuDimension * ivDimension) {
        return;
    }

    glPushName(iuDimension * ivDimension + 1);
    glDisable(GL_LIGHTING);
    Util::myGlColor3f(1.0f, 1.0f, 1.0f);
    if (TheApp->GetHandleMeshAlways() || 
        (_scene->getSelectedHandlesSize() == 0)) {
        for (int i = 0; i < iuDimension; i++) {
            glBegin(GL_LINE_STRIP);
            for (int j = 0; j < ivDimension; j++) {
                const float *v = getControlPoints()->getValue(i + 
                                                              j * iuDimension);
                float w = weight()->getValue(i + j * iuDimension);
                glVertex3f(v[0] / w, v[1] / w, v[2] / w);
            }
            glEnd();
        }
        for (int j = 0; j < ivDimension; j++) {
            glBegin(GL_LINE_STRIP);
            for (int i = 0; i < iuDimension; i++) {
                const float *v = getControlPoints()->getValue(i + 
                                                              j * iuDimension);
                float w = weight()->getValue(i + j * iuDimension);
                glVertex3f(v[0] / w, v[1] / w, v[2] / w);
            }
            glEnd();
        }
    }
    state.startDrawHandles();
    for (int ci = 0; ci < iuDimension * ivDimension; ci++) {
        state.setHandleColor(_scene, ci);
        glLoadName(ci);
        state.drawHandle(Vec3f(getControlPoints()->getValue(ci)) / 
                               weight()->getValue(ci));
    }
    state.endDrawHandles();
    glPopName();
    glEnable(GL_LIGHTING);
}

Vec3f
NodeNurbsSurface::getHandle(int handle, int *constraint,
                            int *field)
{
    *field = controlPoint_Field();

    if (handle >= 0 && handle < getControlPoints()->getSize() / 3) {
        Vec3f ret((Vec3f)getControlPoints()->getValue(handle)
                         / weight()->getValue(handle));
        TheApp->PrintMessageWindowsVertex(IDS_VERTEX_SELECTED, "controlPoint", 
                                          handle);
        return ret;
    } else {
        return Vec3f(0.0f, 0.0f, 0.0f);
    }
}

void
NodeNurbsSurface::setHandle(MFVec3f *value, int handle, float newWeight,
                            const Vec3f &newV, const Vec3f &oldV,
                            bool already_changed)
{
    bool changed = false;
    MFVec3f *newValue = (MFVec3f *) value->copy();

    if (_scene->getXSymetricMode()) {
        float epsilon = TheApp->GetHandleEpsilon();
        int numPoints = newValue->getSize() / 3; 
        for (int i = 0; i < numPoints; i++) {
            if (i != handle) {
                Vec3f vPoint = getControlPoints()->getValue(i);
                float wPoint = weight()->getValue(i);
                float w = wPoint;
                if (wPoint != 0)
                   vPoint = vPoint / wPoint;
                if (newWeight != 0)
                   w = w / newWeight;
                if (   (fabs(vPoint.z - oldV.z) < epsilon) 
                    && (fabs(vPoint.y - oldV.y) < epsilon)) {
                    if (fabs(vPoint.x + oldV.x) < epsilon) {
                        changed = true;
                        if (fabs(oldV.x) < epsilon)
                            newValue->setValue(i * 3, 0);
                        else
                            newValue->setValue(i * 3, - newV.x * w);
                        newValue->setValue(i * 3+1, newV.y * w);
                        newValue->setValue(i * 3+2, newV.z * w);
                    } else if (fabs(vPoint.x - oldV.x) < epsilon) {
                        changed = true;
                        if (fabs(oldV.x) < epsilon)
                            newValue->setValue(i * 3,   0);
                        else
                            newValue->setValue(i * 3, newV.x * w);
                        newValue->setValue(i * 3+1, newV.y * w);
                        newValue->setValue(i * 3+2, newV.z * w);
                    }         
                }                 
            }
        }
    }
    if (already_changed)
        changed = true;

    if (changed) {
        _meshDirty = true;
        if (_x3d) {
            NodeCoordinate *pointNode = (NodeCoordinate *)
                                        controlPointX3D()->getValue();
            if (pointNode)
                _scene->setField(pointNode, pointNode->point_Field(), newValue);
        } else
            _scene->setField(this, controlPoint_Field(), newValue);
    }
}

void
NodeNurbsSurface::setHandle(float newWeight, 
                            const Vec3f &newV, const Vec3f &oldV)
{
    setHandle(getControlPoints(), -1, newWeight, newV, oldV);
}

NodeNurbsGroup *
NodeNurbsSurface::findNurbsGroup()
{
    if (hasParent()) {
        Node* parent = getParent();
        if (parent->getType() == VRML_NURBS_GROUP) {
            return (NodeNurbsGroup *) parent;
        } else if (parent->getType() == VRML_SHAPE)
            if (parent->hasParent())
                if (parent->getParent()->getType() == VRML_NURBS_GROUP) {
                    return (NodeNurbsGroup *) parent->getParent();    
               } 
    }
    return NULL;
}

NodeNurbsSet *
NodeNurbsSurface::findNurbsSet()
{
    if (hasParent()) {
        Node* parent = getParent();
        if (parent->getType() == X3D_NURBS_SET) {
            return (NodeNurbsSet *) parent;
        } else if (parent->getType() == VRML_SHAPE)
            if (parent->hasParent())
                if (parent->getParent()->getType() == X3D_NURBS_SET) {
                    return (NodeNurbsSet *) parent->getParent();    
               } 
    }
    return NULL;
}

void
NodeNurbsSurface::setHandle(int handle, const Vec3f &v)
{
    MFVec3f    *newValue = new MFVec3f(*getControlPoints());

    float epsilon = TheApp->GetHandleEpsilon();
    int numPoints = getControlPoints()->getSize() / 3; 
    if (handle >= 0 && handle < numPoints) {
        float w = weight()->getValue(handle);
        Vec3f oldV = getControlPoints()->getValue(handle);
        if (w != 0)
            oldV = oldV / w;
        Vec3f newV = v * w;
        if (_scene->getXSymetricMode() && (fabs(oldV.x) < epsilon))
            newValue->setValue(handle * 3, 0);
        else
            newValue->setValue(handle * 3, newV.x);
        newValue->setValue(handle * 3+1, newV.y);
        newValue->setValue(handle * 3+2, newV.z);
        // set other handles for symetric modelling
        // which also snap handles at the same place
        setHandle(newValue, handle, w, newV, oldV, true);
        // search for NurbsGroup nodes and set handles
        if (_scene->getXSymetricMode()) {
            NodeNurbsGroup *nurbsGroup = findNurbsGroup();
            if (nurbsGroup)
               nurbsGroup->setHandle(this, w, newV, oldV);
            else {
               NodeNurbsSet *nurbsSet = findNurbsSet();
               if (nurbsSet)
                   nurbsSet->setHandle(this, w, newV, oldV);
            }
        }
    }
}

void
NodeNurbsSurface::setField(int index, FieldValue *value)
{
    _meshDirty = true;
    Node::setField(index, value);
}

int
NodeNurbsSurface::findSpan(int dimension, int order, float u,
                           const float knots[])
{
    int low, mid, high;
    int n = dimension + order - 1;

    if (u >= knots[n]) {
        return n - order;
    }
    low = order - 1;
    high = n - order + 1;

    mid = (low + high) / 2;

    int oldLow = low;
    int oldHigh = high;
    int oldMid = mid;
    while (u < knots[mid] || u >= knots[mid+1]) {
        if (u < knots[mid]) {
            high = mid;
        } else {
            low = mid;
        }
        mid = (low+high)/2;

        // emergency abort of loop, otherwise a endless loop can occure
        if ((low == oldLow) && (high == oldHigh) && (mid == oldMid))
            break;

        oldLow = low;
        oldHigh = high;
        oldMid = mid;
    }
    return mid;
}

void
NodeNurbsSurface::basisFuns(int span, float u, int order,
                            const float knots[], float basis[],
                            float deriv[])
{
    float *left = (float *) malloc(order * sizeof(float));
    float *right = (float *) malloc(order * sizeof(float));

    if ((left==NULL) || (right==NULL))
       return;
    basis[0] = 1.0f;
    for (int j = 1; j < order; j++) {
        left[j] = u - knots[span+1-j];
        right[j] = knots[span+j]-u;
        float saved = 0.0f, dsaved = 0.0f;
        for (int r = 0; r < j; r++) {
            if ((right[r+1] + left[j-r]) == 0) {
                free(left);
                free(right);
                return;
            }
            float temp = basis[r] / (right[r+1] + left[j-r]);
            basis[r] = saved + right[r+1] * temp;
            deriv[r] = dsaved - j * temp;
            saved = left[j-r] * temp;
            dsaved = j * temp;
        }
        basis[j] = saved;
        deriv[j] = dsaved;
    }
    free(left);
    free(right);
}

Vec3f
NodeNurbsSurface::surfacePoint(int uDimension, int uOrder, const float uKnots[],
                               int vDimension, int vOrder, const float vKnots[],
                               const Vec3f controlPoints[],
                               const float weight[], float u, float v,
                               Vec3f &normal)
{
    float *uBasis = (float *) malloc(uOrder * sizeof(float));
    float *vBasis = (float *) malloc(vOrder * sizeof(float));
    float *uDeriv = (float *) malloc(uOrder * sizeof(float));
    float *vDeriv = (float *) malloc(vOrder * sizeof(float));

    int uSpan = findSpan(uDimension, uOrder, u, uKnots);
    int vSpan = findSpan(vDimension, vOrder, v, vKnots);

    basisFuns(uSpan, u, uOrder, uKnots, uBasis, uDeriv);
    basisFuns(vSpan, v, vOrder, vKnots, vBasis, vDeriv);

    int uBase = uSpan-uOrder+1;
    int vBase = vSpan-vOrder+1;

    int index = vBase*uDimension + uBase;
    Vec3f S(0.0f, 0.0f, 0.0f), du(0.0f, 0.0f, 0.0f), dv(0.0f, 0.0f, 0.0f);
    float w = 0.0f, duw = 0.0f, dvw = 0.0f;
    for (int j = 0; j < vOrder; j++) {
        for (int i = 0; i < uOrder; i++) {
            float gain = uBasis[i] * vBasis[j];
            float dugain = uDeriv[i] * vBasis[j];
            float dvgain = uBasis[i] * vDeriv[j];
            S += controlPoints[index] * gain;
            w += weight[index] * gain;
            du += controlPoints[index] * dugain;
            dv += controlPoints[index] * dvgain;
            duw += weight[index] * dugain;
            dvw += weight[index] * dvgain;
            index++;
        }
        index += uDimension - uOrder;
    }
    S = S / w;
    Vec3f un = (du - S * duw) / w;
    Vec3f vn = (dv - S * dvw) / w;
    normal = un.cross(vn);
    normal.normalize();
    free(uBasis);
    free(vBasis);
    free(uDeriv);
    free(vDeriv);
    return S;
}

int
NodeNurbsSurface::writeProto(int f)
{
    if (_x3d)
        return writeX3dProto(f);
    RET_ONERROR( mywritestr(f ,"EXTERNPROTO NurbsSurface[\n") )    
    TheApp->incSelectionLinenumber();
    RET_ONERROR( writeProtoArguments(f) )
    RET_ONERROR( mywritestr(f ," ]\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ,"[\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ," \"urn:web3d:vrml97:node:NurbsSurface\",\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ," \"urn:inet:blaxxun.com:node:NurbsSurface\",\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ," \"urn:ParaGraph:NurbsSurface\",\n") )
    TheApp->incSelectionLinenumber();
#ifdef HAVE_VRML97_AMENDMENT1_PROTO_URL
    RET_ONERROR( mywritestr(f ," \"") )
    RET_ONERROR( mywritestr(f ,HAVE_VRML97_AMENDMENT1_PROTO_URL) )
    RET_ONERROR( mywritestr(f ,"/NurbsSurfacePROTO.wrl") )
    RET_ONERROR( mywritestr(f ,"\"\n") )
    TheApp->incSelectionLinenumber();
#else
    RET_ONERROR( mywritestr(f ," \"NurbsSurfacePROTO.wrl\",\n") )
    TheApp->incSelectionLinenumber();
#endif
    RET_ONERROR( mywritestr(f ," \"http://129.69.35.12/dune/docs/vrml97Amendment1/NurbsSurfacePROTO.wrl\"\n") )
    TheApp->incSelectionLinenumber();
    RET_ONERROR( mywritestr(f ,"]\n") )
    TheApp->incSelectionLinenumber();
    return 0;
}

void
NodeNurbsSurface::rotate(SFRotation rot)
{
    int         numPoints = getControlPoints()->getSize() / 3;

    for (int i = 0; i < numPoints; i++) {
        Vec3f vPoint = getControlPoints()->getValue(i);
        vPoint = rot.getQuat() * vPoint;
        getControlPoints()->setSFValue(i, new SFVec3f(vPoint));
    }        
}

void
NodeNurbsSurface::flip(int index)
{
    if (getControlPoints())
        getControlPoints()->flip(index);
    SFBool *bccw = new SFBool(!(ccw()->getValue()));
    ccw(bccw);
    _meshDirty = true;    
}

void
NodeNurbsSurface::swap(int fromTo)
{
    if (getControlPoints())
        getControlPoints()->swap(fromTo);
    SFBool *bccw = new SFBool(!(ccw()->getValue()));
    ccw(bccw);
    _meshDirty = true;    
}

void 
NodeNurbsSurface::linearUknot(void)
{
    int i;
    int iuOrder = uOrder()->getValue();
    int iuDimension = uDimension()->getValue();
    float *uknots = new float[iuOrder + iuDimension]; 
    for(i = 0; i < iuOrder; i++) {
        uknots[i] = 0.0f;
        uknots[i + iuDimension] = (float) (iuDimension - iuOrder + 1);
    }
    for(i = 0; i < (iuDimension - iuOrder); i++) {
        uknots[i + iuOrder] = (float)(i + 1);  
    } 
    for (i = 0; i < (iuOrder + iuDimension); i++)
        uknots[i] /= (iuDimension - iuOrder + 1);
    _scene->setField(this, uKnot_Field(), 
                     new MFFloat(uknots, iuOrder + iuDimension));
    _meshDirty = true;    
}

void 
NodeNurbsSurface::linearVknot(void)
{
    int i;
    int ivOrder = vOrder()->getValue();
    int ivDimension = vDimension()->getValue();
    float *vknots = new float[ivOrder + ivDimension]; 
    for(i = 0; i < ivOrder; i++) {
        vknots[i] = 0.0f;
        vknots[i + ivDimension] = (float) (ivDimension - ivOrder + 1);
    }
    for(i = 0; i < (ivDimension - ivOrder); i++) {
        vknots[i + ivOrder] = (float)(i + 1);  
    } 
    for (i = 0; i < (ivOrder + ivDimension); i++)
        vknots[i] /= (ivDimension - ivOrder + 1);
    _scene->setField(this, vKnot_Field(), 
                     new MFFloat(vknots, ivOrder + ivDimension));
    _meshDirty = true;    
}

Node*
NodeNurbsSurface::degreeElevate(int newUDegree, int newVDegree)
{
  
  //degree elevate a nurbs surface,
  
  if(newUDegree < ((uOrder()->getValue())-1)){
    return NULL;
  }
  if(newVDegree < ((vOrder()->getValue())-1)){
    return NULL;
  }
  
  NodeNurbsSurface *node = (NodeNurbsSurface *)
                         _scene->createNode("NurbsSurface");

  if((newUDegree >= ((uOrder()->getValue())-1)) && 
     (newVDegree >= ((vOrder()->getValue())-1))){
    
    //load old values
    int i;
    int tuDimension = uDimension()->getValue();
    int tvDimension = vDimension()->getValue();
    Vec3f *tPoints = new Vec3f[tuDimension * tvDimension];
    float *tWeights = new float[tuDimension * tvDimension];
    int tuOrder = uOrder()->getValue();
    int tvOrder = vOrder()->getValue();
    int uKnotSize = uKnot()->getSize();
    int vKnotSize = vKnot()->getSize();
    Array<float> tuKnots(uKnotSize);
    Array<float> tvKnots(vKnotSize);
    int tuDegree = tuOrder - 1;
    int tvDegree = tvOrder - 1;
    int tuUpDegree = newUDegree - tuDegree;
    int tvUpDegree = newVDegree - tvDegree;
    
    for (i=0; i<(tuDimension*tvDimension); i++){
      tPoints[i] = getControlPoints()->getValue(i);
      tWeights[i] =weight()->getValue(i);
    }

    for (i=0; i<uKnotSize; i++){
      tuKnots[i] = uKnot()->getValue(i);
    }
    for (i=0; i<vKnotSize; i++){
      tvKnots[i] = vKnot()->getValue(i);
    }

    //elevate surface
    NurbsSurfaceDegreeElevate elevatedSurface(tPoints, tWeights, 
          tuKnots, tvKnots, tuDimension, tvDimension, tuDegree, tvDegree, 
          tuUpDegree, tvUpDegree);

    //load new node
    
    int newUDimension = elevatedSurface.getUDimension();
    int newVDimension = elevatedSurface.getVDimension();
    float *newControlPoints = new float[newUDimension * newVDimension *3];
    float *newWeights = new float[newUDimension * newVDimension];
    float *newUKnots = new float[elevatedSurface.getUKnotSize()];
    float *newVKnots = new float[elevatedSurface.getVKnotSize()];
    int newUOrder = newUDegree + 1;
    int newVOrder = newVDegree + 1;
    
    for(i=0; i<(newUDimension * newVDimension); i++){
      newControlPoints[(i*3)] = elevatedSurface.getControlPoints(i).x;
      newControlPoints[(i*3)+1] = elevatedSurface.getControlPoints(i).y;
      newControlPoints[(i*3)+2] = elevatedSurface.getControlPoints(i).z;
      newWeights[i] = elevatedSurface.getWeights(i);
    }
    for(i=0; i<(elevatedSurface.getUKnotSize()); i++){
      newUKnots[i] = elevatedSurface.getUKnots(i);
    }
    for(i=0; i<(elevatedSurface.getVKnotSize()); i++){
      newVKnots[i] = elevatedSurface.getVKnots(i);
    }

   
    node->setField(node->uDimension_Field(), new SFInt32(newUDimension));
    node->setField(node->vDimension_Field(), new SFInt32(newVDimension));
    node->uKnot(new MFFloat(newUKnots, newUDimension + newUOrder));
    node->vKnot(new MFFloat(newVKnots, newVDimension + newVOrder));
    node->setField(node->uOrder_Field(), new SFInt32(newUOrder));
    node->setField(node->vOrder_Field(), new SFInt32(newVOrder));
    node->createControlPoints(new MFVec3f(newControlPoints, 
                                          newUDimension * newVDimension * 3));
    node->weight(new MFFloat(newWeights, newUDimension * newVDimension));
    node->uTessellation(new SFInt32(uTessellation()->getValue()));
    node->vTessellation(new SFInt32(vTessellation()->getValue()));
    node->texCoord(new SFNode(texCoord()->getValue()));
    node->solid(new SFBool(solid()->getValue()));
    node->ccw(new SFBool(ccw()->getValue()));
    
    return node;
  }
  return NULL;
}

void *
NodeNurbsSurface::initializeData(void)
{
    int size = getControlPoints()->getSFSize();

    if (size != uDimension()->getValue() * vDimension()->getValue())
        return NULL;

    return new Vec3f[size];
}

void    
NodeNurbsSurface::loadDataFromInterpolators(void *data, Interpolator *inter,
                                            int field, float key)
{
    inter->interpolate(key, (float *)data);
}

void    
NodeNurbsSurface::createMeshFromData(void* data, bool optimize)
{
    _meshDirty = true;
    createMesh((Vec3f *)data, optimize);
}

void    
NodeNurbsSurface::finalizeData(void* data)
{
    delete [] (Vec3f *)data;
}

void 
NodeNurbsSurface::flatten(int direction, bool zero)
{
    MFVec3f    *newValue = (MFVec3f *) getControlPoints()->copy();
 
    if (newValue->getSFSize() == 0)
        return;

    _meshDirty = true;
 
    float value = 0;

    if (!zero) {
        for (int i = 0; i < newValue->getSFSize(); i++)
            value += newValue->getValue(i)[direction] / newValue->getSFSize();
    }
    for (int i = 0; i < newValue->getSFSize(); i++) {
        SFVec3f vec(newValue->getValue(i));
        vec.setValue(direction, value);
        newValue->setSFValue(i, &vec);
    } 
    if (_x3d) {
        NodeCoordinate *coord = (NodeCoordinate *)controlPointX3D()->getValue();
        if (coord != NULL)
            _scene->setField(coord, coord->point_Field(), newValue);
    } else
        _scene->setField(this, controlPoint_Field(), newValue);
}

Node *
NodeNurbsSurface::convert2X3d(void)
{
    if (_x3d == true)
        return NULL;
    MFVec3f *points = getControlPoints();
    NodeCoordinate *node = (NodeCoordinate *)_scene->createNode("Coordinate");
    int len = points->getSize();
    float *floats = new float[len];
    for (int i = 0; i < len; i++)
        floats[i] = points->getValues()[i];
    node->point(new MFVec3f(floats, len));
    SFNode *old = this->controlPointX3D();
    if (old != NULL)
        if (old->getValue() != NULL)
            _scene->execute(new MoveCommand(old->getValue(), 
                                            this, controlPointX3D_Field(),
                                            NULL, -1));
    _scene->execute(new MoveCommand(node, NULL, -1, this, 
                                    controlPointX3D_Field()));
    _scene->changeRoutes(node, node->point_Field(), this, controlPoint_Field(),
                         false);
    NodeUpdate *hint= new NodeUpdate(this, NULL, 0);
    _scene->UpdateViews(NULL, UPDATE_NODE_NAME, (Hint*) hint);
    _scene->UpdateViews(NULL, UPDATE_REDRAW, NULL);
    _x3d = true;
    return NULL;
}

Node *
NodeNurbsSurface::convert2Vrml(void) 
{
    if (_x3d == false)
        return NULL;
    NodeCoordinate *node = (NodeCoordinate *)controlPointX3D()->getValue();
    _x3d = false;
    if (node != NULL) {
        int len = node->point()->getSize();
        float *floats = new float[len];
        for (int i = 0; i < len; i++)
            floats[i] = node->point()->getValues()[i];
        setControlPoints(new MFVec3f(floats, len));
        _scene->changeRoutes(this, controlPoint_Field(), 
                             node, node->point_Field(), false);
        _scene->execute(new MoveCommand(node, this, controlPointX3D_Field(),
                                        NULL, -1));
    } else
        setControlPoints(new MFVec3f());
    NodeUpdate *hint= new NodeUpdate(this, NULL, 0);
    _scene->UpdateViews(NULL, UPDATE_NODE_NAME, (Hint*) hint);
    _scene->UpdateViews(NULL, UPDATE_REDRAW, NULL);
    return NULL;
}

void
NodeNurbsSurface::backupFieldsAppend(int field)
{
    if ((!_x3d) || (field != controlPoint_Field()))
        _scene->backupFieldsAppend(this, field);
    else {
        NodeCoordinate *coord = (NodeCoordinate *)controlPointX3D()->getValue();
        _scene->backupFieldsAppend(coord, coord->point_Field());
    }
}

bool    
NodeNurbsSurface::validHandle(int handle) 
{ 
    if (_x3d && ((handle < 0) || (handle >= controlPoint()->getSFSize())))
        return false;
    if (!_scene->getXSymetricMode())
        return true;
    return _x3d ? true : !_scene->checkSameOrXSymetricHandle(handle, 
                                                             controlPoint());
}

Node *
NodeNurbsSurface::toNurbsTrimmedSurface(void)
{
    NodeNurbsTrimmedSurface *ret = (NodeNurbsTrimmedSurface *)
                                   _scene->createNode("NurbsTrimmedSurface");
    ret->setField(ret->uDimension_Field(), 
                  new SFInt32(uDimension()->getValue()));
    ret->setField(ret->vDimension_Field(), 
                  new SFInt32(vDimension()->getValue()));
    ret->setField(ret->uOrder_Field(), new SFInt32(uOrder()->getValue()));
    ret->setField(ret->vOrder_Field(), new SFInt32(vOrder()->getValue()));
    ret->uTessellation(new SFInt32(uTessellation()->getValue()));
    ret->vTessellation(new SFInt32(vTessellation()->getValue()));
    ret->texCoord(new SFNode(*(SFNode *)texCoord()));
    ret->solid(new SFBool(solid()->getValue()));
    ret->ccw(new SFBool(ccw()->getValue()));

    MFFloat *weights = weight();
    ret->weight(new MFFloat(((MFFloat *)weights->copy())));

    MFFloat *uknots = uKnot();
    ret->uKnot(new MFFloat(((MFFloat *)uknots->copy())));

    MFFloat *vknots = vKnot();
    ret->vKnot(new MFFloat(((MFFloat *)vknots->copy())));

    MFVec3f *points = NULL;
    points = getControlPoints();
    NodeCoordinate *node = (NodeCoordinate *)
                           _scene->createNode("Coordinate");
    node->point(new MFVec3f(((MFVec3f *)points->copy())));

    SFNode *old = controlPointX3D();
    if (old != NULL)
        if (old->getValue() != NULL)
            _scene->execute(new MoveCommand(old->getValue(), 
                                            this, controlPointX3D_Field(),
                                            NULL, -1));
    _scene->execute(new MoveCommand(node, NULL, -1, ret,
                                    ret->controlPointX3D_Field()));
    _scene->changeRoutes(node, node->point_Field(), ret, 
                         ret->controlPointX3D_Field(), false);        
    return ret;
}

