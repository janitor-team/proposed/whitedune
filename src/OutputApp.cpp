/*
 * OutputApp.cpp
 *
 * Copyright (C) 2003 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <errno.h>
#include "stdafx.h"
#include <math.h>
#include <swt.h>

#include "DuneApp.h"
#include "WonderlandModuleExport.h"
#include "PreferencesApp.h"

#ifndef HAVE_X3DV_TO_X3D_COMMAND
# define HAVE_X3DV_TO_X3D_COMMAND ""
#endif

#ifdef HAVE_CHECK_IN_COMMAND
# define CHECK_IN_COMMAND_ENABLED true
#else
# define CHECK_IN_COMMAND_ENABLED false
# define HAVE_CHECK_IN_COMMAND " "
#endif

#define DEFAULT_INDENT 2
#define DEFAULT_FLOAT_DIGITS 6

#define DEFAULT_AC3D_MATERIAL_NAME "window"
#define DEFAULT_CATT_MATERIAL_NAME "TOTREF"
#define DEFAULT_MATERIAL_NAME_GENERATION MATERIAL_NAME_FULL

#define WONDERLAND_PREFIX "X3d"

OutputApp::OutputApp()
{
    _revisionControlCheckinCommand = NULL;
    _x3dv2X3dCommand = NULL;
    _defaultAc3dMaterialName = NULL;
    _defaultCattMaterialName = NULL;
    _wonderlandModuleExport = NULL;
    _wonderlandModuleExportPath = NULL;
    _wonderlandModuleExportPrefix = NULL;
    _writtenElementsPerJavaArray = -1;
    _cattExportPath = NULL;
}

void
OutputApp::OutputSetDefaults()
{
#ifdef _WIN32
    _keepURLs = true;
#else
    _keepURLs = false;
#endif
    _krFormating = false;
    _indent = DEFAULT_INDENT;
    _floatDigits = DEFAULT_FLOAT_DIGITS;
    _oldFloatDigits = DEFAULT_FLOAT_DIGITS;
    _useEFloatWriteFormat = true;
    _normalsOnPureVRML97 = false;
    _skipMaterialNameBeforeFirstUnderscore = false;
    _skipMaterialNameAfterLastUnderscore = false;
    if (_defaultAc3dMaterialName != NULL)
        free(_defaultAc3dMaterialName);
    _defaultAc3dMaterialName = strdup(DEFAULT_AC3D_MATERIAL_NAME);
    _ac3dExport4Raven = false;
    _ac3dExportConvert2Gif = false;
    if (_defaultCattMaterialName != NULL)
        free(_defaultCattMaterialName);
    _defaultCattMaterialName = strdup(DEFAULT_CATT_MATERIAL_NAME);
    _revisionControlCheckinFlag = CHECK_IN_COMMAND_ENABLED;
    if (_revisionControlCheckinCommand != NULL)
        free(_revisionControlCheckinCommand);
    _revisionControlCheckinCommand = strdup(HAVE_CHECK_IN_COMMAND);
    if (_x3dv2X3dCommand != NULL)
        free(_x3dv2X3dCommand);
    _x3dv2X3dCommand = strdup(HAVE_X3DV_TO_X3D_COMMAND);
    _wonderlandModuleExportManyClasses = false;
}

void
OutputApp::OutputLoadPreferences()
{
    assert(TheApp != NULL);

    OutputSetDefaults();

#ifdef _WIN32
    _keepURLs = TheApp->GetBoolPreference("KeepURLs", true);    
#else
    _keepURLs = TheApp->GetBoolPreference("KeepURLs", false);    
#endif 
    _indent = TheApp->GetIntPreference("Indent", DEFAULT_INDENT); 
    _floatDigits = TheApp->GetIntPreference("FloatDigits", 
                                            DEFAULT_FLOAT_DIGITS);
    _krFormating = TheApp->GetBoolPreference("KrFormating", false);    
    _includeProtos = TheApp->GetBoolPreference("IncludeProtos", false);    
    _compress = TheApp->GetBoolPreference("Compress", false);    
    _normalsOnPureVRML97 = TheApp->GetBoolPreference("NormalsOnPureVRML97", 
                                                     true);    
    _keepURLs = TheApp->GetBoolPreference("KeepURLs", true);    
    _skipMaterialNameBeforeFirstUnderscore = TheApp->GetBoolPreference(
          "SkipMaterialNameBeforeFirstUnderscore", false);
    _skipMaterialNameAfterLastUnderscore = TheApp->GetBoolPreference(
          "SkipMaterialNameAfterLastUnderscore", false);
    if (_defaultAc3dMaterialName != NULL)
        free(_defaultAc3dMaterialName);
    _defaultAc3dMaterialName = strdup(TheApp->GetPreference(
                                      "DefaultAc3dMaterialName",
                                      DEFAULT_AC3D_MATERIAL_NAME));
    _ac3dExport4Raven = TheApp->GetBoolPreference("Ac3dExport4Raven", false);
#ifdef HAVE_IMAGE_CONVERTER
    _ac3dExportConvert2Gif = TheApp->GetBoolPreference("Ac3dExportConvert2Gif",
                                                       true);
#else
    _ac3dExportConvert2Gif = false;
#endif
    if (_defaultCattMaterialName != NULL)
        free(_defaultCattMaterialName);
    _defaultCattMaterialName = strdup(TheApp->GetPreference(
                                      "DefaultCattMaterialName",
                                      DEFAULT_CATT_MATERIAL_NAME));
    _cattExportSrcRec = TheApp->GetBoolPreference("CattExportSrcRec", true);    
    _revisionControlCheckinFlag = TheApp->GetBoolPreference(
                                  "UseRevisionControl",
                                  CHECK_IN_COMMAND_ENABLED); 
    if (_revisionControlCheckinCommand != NULL)
        free(_revisionControlCheckinCommand);
    _revisionControlCheckinCommand = strdup(TheApp->GetPreference(
                                            "RevisionControlCheckInCommand",
                                            HAVE_CHECK_IN_COMMAND));
    if (_x3dv2X3dCommand != NULL)
        free(_x3dv2X3dCommand);
    _x3dv2X3dCommand = strdup(TheApp->GetPreference("X3dv2X3dCommand",
                                                    HAVE_X3DV_TO_X3D_COMMAND));
    if (_wonderlandModuleExportPath != NULL)
        free(_wonderlandModuleExportPath);
    _wonderlandModuleExportPath = strdup(TheApp->GetPreference(
                                           "WonderlandModuleExportPath",
                                           ""));
    if (_wonderlandModuleExportPrefix != NULL)
        free(_wonderlandModuleExportPrefix);
    _wonderlandModuleExportPrefix = strdup(TheApp->GetPreference(
                                           "WonderlandModuleExportPrefix",
                                           WONDERLAND_PREFIX));
    _wonderlandModuleExportManyClasses = TheApp->GetBoolPreference(
                                         "WonderlandModuleExportManyClasses",
                                         false);
    TheApp->updatePrefix();
    if (_cattExportPath != NULL)
        free(_cattExportPath);
    _cattExportPath = strdup(TheApp->GetPreference("CattExportPath", ""));
}

void
OutputApp::OutputSavePreferences()
{
    assert(TheApp != NULL);
 
    TheApp->SetBoolPreference("KeepURLs", _keepURLs);
    TheApp->SetIntPreference("Indent", _indent);
    TheApp->SetIntPreference("FloatDigits", _floatDigits); 
    TheApp->SetBoolPreference("KrFormating", _krFormating);
    TheApp->SetBoolPreference("IncludeProtos", _includeProtos);
    TheApp->SetBoolPreference("Compress", _compress);
    TheApp->SetBoolPreference("NormalsOnPureVRML97", _normalsOnPureVRML97);
    TheApp->SetPreference("DefaultAc3dMaterialName",
                          _defaultAc3dMaterialName);
    TheApp->SetBoolPreference("Ac3dExport4Raven",
                              _ac3dExport4Raven);
    TheApp->SetBoolPreference("Ac3dExportConvert2Gif",
                              _ac3dExportConvert2Gif);
    TheApp->SetBoolPreference("SkipMaterialNameBeforeFirstUnderscore",
                              _skipMaterialNameBeforeFirstUnderscore);
    TheApp->SetBoolPreference("SkipMaterialNameAfterLastUnderscore",
                              _skipMaterialNameAfterLastUnderscore);
    TheApp->SetPreference("DefaultCattMaterialName",
                          _defaultCattMaterialName);
    TheApp->SetBoolPreference("CattExportSrcRec", _cattExportSrcRec);
    TheApp->SetBoolPreference("UseRevisionControl", _revisionControlCheckinFlag);
    TheApp->SetPreference("RevisionControlCheckInCommand", 
                          _revisionControlCheckinCommand);
    TheApp->SetPreference("X3dv2X3dCommand", _x3dv2X3dCommand);
    TheApp->SetPreference("WonderlandModuleExportPath", 
                          _wonderlandModuleExportPath);
    TheApp->SetPreference("WonderlandModuleExportPrefix", 
                          _wonderlandModuleExportPrefix);
    TheApp->SetBoolPreference("WonderlandModuleExportManyClasses", 
                              _wonderlandModuleExportManyClasses);

    TheApp->SetPreference("CattExportPath", 
                          _cattExportPath);
}
 
bool parseCommandlineArgumentOutput(int & i,int argc, char** argv)
{
    bool found = true;
    if (strcmp(argv[i],"-krFormating")==0) {
        TheApp->SetkrFormating(true);
    } else if (strcmp(argv[i],"-indent")==0) {
        int indent;
        if (i++>=argc) return found;
        if (sscanf(argv[i],"%d",&indent)==1)
            TheApp->SetIndent(indent);
    } else if (strcmp(argv[i],"-floatDigits")==0) {
        int floatDigits;
        if (i++>=argc) return found;
        if (sscanf(argv[i],"%d",&floatDigits)==1)
            TheApp->SetFloatDigits(floatDigits);
    } else if (strcmp(argv[i],"-noEFloatWriteFormat")==0) {
        TheApp->forbidEFloatWriteFormat();
    } else
        return false;
    return found;
}


void
OutputApp::SetFloatDigits(int value)
{
   set_number_of_digits(value);
   _floatDigits = value;
}

void
OutputApp::forbidEFloatWriteFormat()
{
    _useEFloatWriteFormat = false;
}

void
OutputApp::enableEFloatWriteFormat(int writeFlags)
{
    if (_useEFloatWriteFormat)
        useEFloatWriteFormat(1);
    if (writeFlags & X3D_4_WONDERLAND)
        useEFloatWriteFormat(1);        
}

void
OutputApp::disableEFloatWriteFormat(int writeFlags)
{
    if (_useEFloatWriteFormat)
        useEFloatWriteFormat(0);
    if (writeFlags & X3D_4_WONDERLAND)
        useEFloatWriteFormat(0);        
}

void
OutputApp::useEFloatWriteFormat(int flag)
{ 
    if (!_useEFloatWriteFormat)
        return;
    if (flag) {
        _floatDigits = _oldFloatDigits;
        set_Efloat_format(0);
    } else {
        _oldFloatDigits = _floatDigits;
        SetFloatDigits(9);
        set_Efloat_format(1);
    }
}

int 
OutputApp::writeWonderlandModule(const char* directory, const char* name, 
                                 Scene* scene, bool manyClasses)
{
    _wonderlandModuleExport = new WonderlandModuleExport();
    int ret =_wonderlandModuleExport->write(directory, name, scene, 
                                            manyClasses);
    delete _wonderlandModuleExport;
    _wonderlandModuleExport = NULL;
    return ret;
}

int 
OutputApp::writeWonderlandModuleArtPath(int filedes, const char *fileName) 
{
    if (_wonderlandModuleExport != NULL)
        return _wonderlandModuleExport->writeArtPath(filedes, fileName);
    swDebugf("Internal error: tried to export to a WonderlandModule"); 
    swDebugf(" but function is not uninitialised\n");
    return -1;         
}

void                
OutputApp::SetWonderlandModuleExportPrefix(char *prefix)
{
    if (_wonderlandModuleExportPrefix != NULL)
        free(_wonderlandModuleExportPrefix);
    _wonderlandModuleExportPrefix = strdup(prefix);
    TheApp->updatePrefix();
}

