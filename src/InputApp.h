/*
 * InputApp.h
 *
 * Copyright (C) 2007 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _INPUT_APP_H
#define _INPUT_APP_H

extern bool parseCommandlineArgumentInput(int & i,int argc, char** argv);

class InputApp {
public:
                        InputApp();

    char               *GetX3d2X3dvCommand(void)
                           { return _x3d2X3dvCommand;}
    void                SetX3d2X3dvCommand(char *command)
                           {
                           if (_x3d2X3dvCommand != NULL)
                               free(_x3d2X3dvCommand);
                           _x3d2X3dvCommand = strdup(command);
                           }

    bool                useInternalXmlParser()
                           { return _useInternalXmlParser; }
    void                setUseInternalXmlParser(bool flag) 
                           { _useInternalXmlParser = flag; }

    void                InputSetDefaults();

    void                InputLoadPreferences();
    void                InputSavePreferences();

    int                 getX3dSize(void) { return _x3dSize; }
    void                setX3dSize(int sizeInMB) 
                           { _x3dSize = sizeInMB * 1024 * 1024; }

private:
    char               *_x3d2X3dvCommand;
    bool                _useInternalXmlParser;
    int                 _x3dSize;
};

#endif

