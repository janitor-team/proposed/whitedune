whitedune (0.30.10-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS due new binary matching behavior on grep (Closes: #814976)

 -- Raúl Benencia <rul@kalgan.cc>  Mon, 07 Mar 2016 07:56:28 -0800

whitedune (0.30.10-2) unstable; urgency=medium

  * Team upload.

  [ Tobias Frost ]
  * Fix "FTBFS with libpng 1.5", applying the patch from Nobuhiro
    (Closes: #638812)
  * Add d/clean to remove created files

  [ Sebastian Ramacher ]
  * Acknowledge NMUs. (Closes: #676335)
  * debian/control:
    - Run wrap-and-sort.
    - Build debhelper Build-Dep to (>= 9).
    - Update Vcs-*.
    - Bump Standards-Version.
  * debian/compat: Bump to 9.
  * debian/rules:
    - Fix stripping.
    - Pass all flags from dpkg-buildflags.
    - Fix binary-arch target. (Closes: #563266)
    - Some clean up.
  * debian/patches: Give patches a meaningful names.

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 09 Jan 2016 21:48:03 +0100

whitedune (0.30.10-1.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Drop unnecessary build dep on automake1.9. (Closes: #724444)

 -- Eric Dorland <eric@debian.org>  Fri, 14 Mar 2014 23:43:11 -0400

whitedune (0.30.10-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Transition from lesstif2 to motif (Closes: #714678).

 -- Luk Claes <luk@debian.org>  Sat, 14 Sep 2013 18:26:47 +0200

whitedune (0.30.10-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control: Update Build-Depends:
    - libjpeg62-dev -> libjpeg-dev  (Closes: #647104)
    - libpng12-dev -> libpng-dev    (Closes: #662548)

 -- Bill Allombert <ballombe@debian.org>  Tue, 05 Jun 2012 23:08:32 +0200

whitedune (0.30.10-1) unstable; urgency=low

  * New upstream version
  * debian/control:
    - Comaintenance set to pkg-multimedia team
    - recommends fonts (Close:#563925)
    - updated standards and fix lintian warning (src)
  * configure.ac: undepends on libxp (Closes: #623652)
  * ignore generated files

 -- Philippe Coval <rzr@gna.org>  Fri, 29 Apr 2011 19:23:37 +0200

whitedune (0.28.14-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Set urgency to 'medium' for RC bug fixes.
  * debian/control: Add 'xfonts-100dpi' to Recommends.  Fixes segmentation
    fault on startup. (Closes: #550459)
  * src/parser.y: Apply patch from upstream version 0.29beta1391 to fix
    bison 3.4.1 compatibility problem.  Fixes FTBFS. (Closes: #562683)

 -- Tim Retout <tim@retout.co.uk>  Thu, 24 Dec 2009 15:51:59 +0000

whitedune (0.28.14-1) unstable; urgency=low

  * New upstream release
  * prefer x-www-browser over netscape (LP:#399981)
  * debian/control :
    - Update dependencies (Closes: #515402)
    - Update email to non bouncing domain
    - Update upstream URL, description
    - Update standards to current ones
  * debian/changelog :
    - Fix licences versions

 -- Philippe Coval <rzr@gna.org>  Sat, 25 Jul 2009 18:39:55 +0200

whitedune (0.28.13-1) unstable; urgency=low

  * NEW: upstream release (which fix buffer overflow exploit)
  * minor fixes : copyright, manpages, makefile

 -- Philippe Coval <rzr@users.sf.net>  Wed, 02 Jan 2008 10:32:04 +0100

whitedune (0.28.12-1) experimental; urgency=low

  * Intial release (Closes: #400739 )
  * info : package based on previous work done upstream
  (by Thiemo Seufer then J. "MUFTI" Scheurich)

 -- Philippe Coval <rzr@users.sf.net>  Tue, 17 Jul 2007 19:52:00 +0200
