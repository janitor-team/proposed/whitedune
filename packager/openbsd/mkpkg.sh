#!/bin/sh
# set $PREFIX, if you do not want to install to /usr

WTMP=/tmp/mkpkg_$$
mkdir $WTMP || exit 1
export WTMP

if test "X_$PREFIX" = "X_" ; then
PREFIX=/usr
export PREFIX
fi

VERSION=`sh ../../batch/getversion.sh`
export VERSION

(cd ../../.. && cp -r white_dune-$VERSION $WTMP &&
 cd $WTMP/white_dune-$VERSION && 
 sh batch/fix_not_translated_rcfiles.sh &&
 make realclean && 
 ./configure --with-optimization --without-devil \
 --without-sdljoystick --with-vrmlbrowser=mozilla \
 --with-helpurl="$PREFIX/share/doc/white_dune" \
 --with-protobaseurl="$PREFIX/share/doc/white_dune" \
 && make ) || exit 1

mkdir -p $PREFIX/X11R6/bin/ &&
mkdir -p $PREFIX/bin/ &&
cp $WTMP/white_dune-$VERSION/bin/dune $PREFIX/X11R6/bin/ &&
cp $WTMP/white_dune-$VERSION/bin/dune4kids $PREFIX/X11R6/bin/ &&
cp $WTMP/white_dune-$VERSION/bin/illegal2vrml $PREFIX/bin/ &&
chmod 755 $PREFIX/X11R6/bin/dune &&
chmod 755 $PREFIX/X11R6/bin/dune4kids &&
chmod 755 $PREFIX/bin/illegal2vrml &&

mkdir -p $PREFIX/man/man1 &&
cp $WTMP/white_dune-$VERSION/man/dune.1 $PREFIX/man/man1 &&
gzip -9f $PREFIX/man/man1/dune.1 &&
cp $WTMP/white_dune-$VERSION/man/illegal2vrml.1 $PREFIX/man/man1 &&
gzip -9f $PREFIX/man/man1/illegal2vrml.1

chmod 644 $PREFIX/man/man1/dune.1.gz
chmod 644 $PREFIX/man/man1/illegal2vrml.1.gz

mkdir -p $PREFIX/share/doc/white_dune/ &&
cp -r $WTMP/white_dune-$VERSION/docs/* $PREFIX/share/doc/white_dune/

find $PREFIX/share/doc/white_dune/ -type f -print | xargs chmod 644
find $PREFIX/share/doc/white_dune/ -type d -print | xargs chmod 755

echo @name whitedune-$VERSION > $WTMP/pkg-list

if sh -c "ldd $PREFIX/X11R6/bin/dune | grep jpeg > /dev/null" ; then
   echo @depend `pkg_info | egrep ^jpeg- | awk '{print $1}'` >> $WTMP/pkg-list
fi
if sh -c "ldd $PREFIX/X11R6/bin/dune | grep png > /dev/null" ; then
   echo @depend `pkg_info | egrep ^png- | awk '{print $1}'` >> $WTMP/pkg-list
fi
echo @depend `pkg_info | egrep ^Mesa- | awk '{print $1}'` >> $WTMP/pkg-list
echo @depend `pkg_info | egrep ^open-motif- | awk '{print $1}'` >> $WTMP/pkg-list
echo @depend `pkg_info | egrep ^XFree86-libraries- | awk '{print $1}'` >> $WTMP/pkg-list

cat >> $WTMP/pkg-list << EOT
@cwd $PREFIX
X11R6/bin/dune
X11R6/bin/dune4kids
bin/illegal2vrml
man/man1/dune.1.gz
man/man1/illegal2vrml.1.gz
EOT

cp pkg-descr $WTMP
cp pkg-comment $WTMP

(cd $PREFIX && 
 find share/doc/white_dune -type f -print >> $WTMP/pkg-list) 
(cd $PREFIX && 
 find share/doc/white_dune -type d -print | sort -r | xargs -n 1 echo @dirrm >> $WTMP/pkg-list)

(cd /tmp && 
pkg_create -f $WTMP/pkg-list -c $WTMP/pkg-comment -d $WTMP/pkg-descr whitedune-$VERSION.tgz)

mv /tmp/whitedune-$VERSION.tgz /tmp/whitedune-OpenBSD-$VERSION.tgz
echo use \"pkg_add /tmp/whitedune-OpenBSD-$VERSION.tgz\" to install
rm -r $WTMP
